/*
 * main.c
 *
 *  Created on: 04/05/2020
 *      Author: isep
 */

#include <errno.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>





#define NUMBER_OF_PROCESSES 2
#define NUMBER_OF_SEM 2
#define SEM_PRINT_S 0
#define SEM_PRINT_C 1

int cria_filhos(int n) {
	pid_t pid;
	int i;
	for (i = 0; i < n; i++) {
		pid= fork();
		if (pid < 0) {
			return -1;
		} else if (pid == 0) {
			return i + 1;
		}
	}
	return 0;

}

void printMessage(char *message) {
	printf("%s", message);

}

void __semWAIT(sem_t *semaforo) {
	if (sem_wait(semaforo) == -1) {
		perror("Error at sem_wait()!");
		exit(EXIT_FAILURE);
	}
}

void __semPOST(sem_t *semaforo) {
	if (sem_post(semaforo) == -1) {
		perror("Error at sem_post()!");
		exit(EXIT_FAILURE);
	}
}

int main(int argc, char * agrv[]) {
	const char SEMAFORES_NAME[NUMBER_OF_SEM][80] = { "S", "C" };
	const int SEMAFORES_INIT_VALUE[NUMBER_OF_SEM] = { 1, 1 };
	sem_t *semaforos[NUMBER_OF_SEM];

	//abrir semaforos com o valor inicial
	int i;
	for (i = 0; i < NUMBER_OF_SEM; i++) {
		if ((semaforos[i] = sem_open(SEMAFORES_NAME[i], O_CREAT | O_EXCL, 0644,
				SEMAFORES_INIT_VALUE[i])) == SEM_FAILED) {
			perror("Error at sem_open()\n");
			exit(EXIT_FAILURE);
		}
	}

	//criar os filhos

	int id = cria_filhos(NUMBER_OF_PROCESSES);

	//se tiveres no filho #1 print S
	if (id == 1) {
		while (1) {
			__semWAIT(semaforos[SEM_PRINT_S]);
			printMessage("S");
			__semPOST(semaforos[SEM_PRINT_C]);
		}
		exit(EXIT_SUCCESS);
	}
	//se tivermos no filho #2 print C

	if (id == 2) {
		while (1) {
			__semWAIT(semaforos[SEM_PRINT_C]);
			printMessage("C");
			__semPOST(semaforos[SEM_PRINT_S]);
		}
		exit(EXIT_SUCCESS);
	}

	//esperar pelos processos
	for (i = 0; i < NUMBER_OF_PROCESSES; i++) {
		wait(NULL);
	}

	//fechar semaforos

	for (i = 0; i > NUMBER_OF_SEM; i++) {
		if (sem_close(semaforos[i]) == -1) {
			perror("Error at sem_close()\n");
			exit(EXIT_FAILURE);
		}
	}

	//remover semaforos

	for (i = 0; i < NUMBER_OF_SEM; i++) {
		if (sem_unlink(SEMAFORES_NAME[i]) == -1) {
			perror("Error at sem_unlink()!\n");
			exit(EXIT_FAILURE);
		}
	}

	return EXIT_SUCCESS;

}

