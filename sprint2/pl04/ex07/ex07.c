#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fcntl.h>
#include <semaphore.h>

static const int CHILDS = 3;
static const char SEMAFOROS[3][15] = { "/sem_1", "/sem_2", "/sem_3" };
static const int ESTADO_SEMAFORO[3] = { 1, 0, 0 };

int main(void) {

	// Criação dos semaforos
	sem_t* sem[3];
	int i;
	for (i = 0; i < 3; i++) {
		if ((sem[i] = sem_open(SEMAFOROS[i], O_CREAT | O_EXCL, 0644,
				ESTADO_SEMAFORO[i])) == SEM_FAILED) {
			perror("Sem_open failed!\n");
			exit(EXIT_FAILURE);
		}
	}

	pid_t pid[CHILDS];
	for (i = 0; i < CHILDS; i++) {
		pid[i] = fork();
		if (pid[i] < 0) {
			return EXIT_FAILURE;
		} else if (pid[i] == 0) {    // Child Processes
			if (i == 0) {
				sem_wait(sem[0]);
				printf("Sistemas ");
				fflush(stdout);
				sem_post(sem[1]);

				sem_wait(sem[0]);
				printf("a ");
				fflush(stdout);
				sem_post(sem[1]);

				exit(EXIT_SUCCESS);
			} else if (i == 1) {
				sem_wait(sem[1]);
				printf("de ");
				fflush(stdout);
				sem_post(sem[2]);

				sem_wait(sem[1]);
				printf("melhor ");
				fflush(stdout);
				sem_post(sem[2]);

				exit(EXIT_SUCCESS);
			} else if (i == 2) {
				sem_wait(sem[2]);
				printf("Computadores - ");
				fflush(stdout);
				sem_post(sem[0]);

				sem_wait(sem[2]);
				printf("disciplina! \n");
				fflush(stdout);
				sem_post(sem[0]);

				exit(EXIT_SUCCESS);
			}
		}
	}

	for (i = 0; i < CHILDS; i++) { // Wait for all child processes
		wait(NULL);
	}

	for (i = 0; i < 3; i++) { // Unlink all semaphores
		if (sem_unlink(SEMAFOROS[i]) == -1) {
			perror("Sem_unlink failed!\n");
			exit(EXIT_FAILURE);
		}
	}

	return 0;
}

