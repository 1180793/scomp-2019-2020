/*
 * ex_01.h
 *
 *  Created on: 04/05/2020
 *      Author: isep
 */

#ifndef EX_01_H_
#define EX_01_H_

#define int NUMBER_CHILDREN 8
#define int NUMBERS_PER_PROCESS 200
#define void son(int numberChilden, sem_t *leitura, sem_t *escrita);
#define int LEITURA 0
#define int ESCRITA 1




#endif /* EX_01_H_ */
