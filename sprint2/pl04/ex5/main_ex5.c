#include <errno.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char *agrv[]) {
   
    const char* semaforo_fich = "ex05";

    // Open sem
    sem_t *semaforo;
    if ((semaforo = sem_open(semaforo_fich, O_CREAT | O_EXCL, 0644, 0)) == -1) {
        perror("Error at sem_open()!\n");
        exit(-1);
    }
    pid_t pid = fork();
	 if (pid < 0) exit(-1);
            
    if (pid > 0) {
        if (sem_wait(semaforo) == -1) {
            perror("Error at sem_wait()!");
            exit(-1);
        }
        printf("I'm the father!\n");
    }else{
        printf("I'm the child!\n");
        if (sem_post(semaforo) == -1) {
            perror("Error at sem_post()!");
            exit(-1);
        }
        exit(0);
    }
    if (sem_close(semaforo) == -1) {
        perror("Error at sem_close()!\n");
        exit(-1);;
    }
    if (sem_unlink(semaforo_fich) == -1) {
        perror("Error at sem_unlink()!\n");
        exit(-1);
    }

    return 0;
}
