/*
 * startup.c
 *
 *  Created on: 05/05/2020
 *      Author: isep
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <string.h>
#include "header.h"

int main(){

	int fd;
	int ft;
	int r;
	int data_size = sizeof(str);
	str* addr;

	fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	if(fd == -1) {
			perror("shm_open error...");
			exit(EXIT_FAILURE);
	}

	addr = (str*) mmap(NULL, data_size,PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if( addr == MAP_FAILED){
		perror("mmap error...");
		exit(EXIT_FAILURE);
	}
	ft = ftruncate (fd, data_size);
		if(ft == -1){
			perror("ftruncate error...");
			exit(EXIT_FAILURE);
		}

	//Initializes the shared memory area string index with the value 0;
	addr->index = 0;

	r = munmap(addr, data_size);
	if(r == -1){
		perror("munmap error...");
		exit(EXIT_FAILURE);
	}

	return 0;

}
