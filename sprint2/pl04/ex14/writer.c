#include "includes.h"

int main()
{

    const char semaforos_nome[nr_semaforos][80] = {"mutex", "read", "barreira"};
    const int semaforos_valores[nr_semaforos] = {1, 1, 1};
    sem_t *semaforos[nr_semaforos];

    int i;

    for (i = 0; i < nr_semaforos; i++)
    {
        if ((semaforos[i] = sem_open(semaforos_nome[i], O_CREAT, 0644, semaforos_valores[i])) == SEM_FAILED)
        {
            perror("Error at sem_open()!\n");
            exit(EXIT_FAILURE);
        }
    }
    shared_data *shared_data1;
    int fd;

    if ((fd = shm_open("/ex14shm", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR)) == -1)
    {
        perror("shm_open error");
        exit(-1);
    }

    if (ftruncate(fd, sizeof(shared_data1)) == -1)
    {
        perror("ftruncate error");
        exit(-1);
    }

    if ((shared_data1 = (shared_data *)mmap(NULL, sizeof(shared_data1), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) == MAP_FAILED)
    {
        perror("mmap error");
        exit(-1);
    }

    __semWAIT(semaforos[BARREIRA]);
    shared_data1->total_writters++;
    __semWAIT(semaforos[NO_USE]);

    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    printf("WRITER - nr of writers is %d, nr of readers is %d\n", shared_data1->total_writers, shared_data1->total_readers);

    sprintf(shared_data1->string, "pid is %d, time is %s", getpid(), asctime (timeinfo));

    __semPOST(semaforos[BARREIRA]);
    __semPOST(semaforos[NO_USE]);


    return 0;
}
