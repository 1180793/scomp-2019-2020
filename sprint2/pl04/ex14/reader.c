#include "includes.h"


int main()
{

    const char semaforos_nome[nr_semaforos][80] = {"BARREIRA", "NO_USE", "MUTEX"};
    const int SEMAFOROS_INITIAL_VALUE[nr_semaforos] = {1,1,1};
    sem_t *semaforos[nr_semaforos];
s
    int i;

    for (i = 0; i < nr_semaforos; i++)
    {
        if ((semaforos[i] = sem_open(semaforos_nome[i], O_CREAT, 0644, SEMAFOROS_INITIAL_VALUE[i])) == SEM_FAILED)
        {
            perror("Error at sem_open()seller!\n");
            exit(-1);
        }
    }
    shared_data *shared_data1;
    int fd;

    if ((fd = shm_open("/ex14shm", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR)) == -1)
    {
        perror("shm_open error");
        exit(-1);
    }

    if (ftruncate(fd, sizeof(shared_data1)) == -1)
    {
        perror("ftruncate error");
        exit(-1);
    }

    if ((shared_data1 = (shared_data *)mmap(NULL, sizeof(shared_data1), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) == MAP_FAILED)
    {
        perror("mmap error");
        exit(-1);
    }
    
    //garante a prioridade do writer
    __semWAIT(semaforos[BARREIRA]);
    __semPOST(semaforos[BARREIRA]);
    
    
    //unicidade a mexer na memoria
    __semWAIT(semaforos[MUTEX]);

    
    shared_data1->current_readers++;
    shared_data1->total_readers++;

    if(shared_data1->current_readers==1){
        __semWAIT(semaforos[NO_USE]);
    }
    
    
    __semPOST(semaforos[MUTEX]);
    
    printf("%s", shared_data1->string);
    
    
    //nova escrita ptt mutex
    __semWAIT(semaforos[MUTEX]);
     shared_data1->current_readers--;

    
    //se nao houver leitores libertamos o semaforo no use
    if( shared_data1->current_readers==0){
        __semPOST(semaforos[NO_USE]);
    }

    __semPOST(semaforos[MUTEX]);

    return 0;
}
