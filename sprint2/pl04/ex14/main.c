#include "includes.h"

int main(){
    

    int fd;
	shared_data* shared_data1;
	
	if((fd = shm_open("/ex14shm", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1){
		perror("shm_open error");
		exit(-1);
	}
	
	if(ftruncate(fd,sizeof(shared_data1)) == -1){
		perror("ftruncate error");
		exit(-1);
	}
	
	if(( shared_data1 = (shared_data*) mmap(NULL, sizeof(shared_data1), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0)) == MAP_FAILED){
		perror("mmap error");
		exit(-1);
	}

    shared_data1->current_readers=0;
	shared_data1->total_readers=0;
    shared_data1->total_writers=0;

    const char semaforos_name[nr_semaforos][80] = {"BARREIRA", "NO_USE", "MUTEX"};
    const int semaforos_value[nr_semaforos] = {1, 1, 1};
    sem_t *semaforos[nr_semaforos];

    int i;

    for (i = 0; i < nr_semaforos; i++)
    {
        if ((semaforos[i] = sem_open(semaforos_name[i], O_CREAT, 0644, semaforos_value[i])) == SEM_FAILED)
        {
            perror("Error at sem_open()main!\n");
            exit(-1);
        }
    }

    pid_t pid;
    for(i=0; i<writers; i++){
        pid=fork();
		if(pid==0){
			execlp("./writer","./writer",(char*)NULL);
		}
    }

    for(i=0; i<readers; i++){
        pid=fork();
		if(pid==0){
			execlp("./reader","./reader",(char*)NULL);
		}
    }

    for(i=0; i<(readers+writers); i++){
        wait(NULL);
    }

   // Close all semaforos
    for (i = 0; i < nr_semaforos; i++)
    {
        if (sem_close(semaforos[i]) == -1)
        {
            perror("Error at sem_close()!\n");
            exit(-1);
        }
    }

    // Remove semaforos from system
    for (i = 0; i < nr_semaforos; i++)
    {
        if (sem_unlink(semaforos_name[i]) == -1)
        {
            perror("Error at sem_unlink()!\n");
            printf("Error: %s\n", strerror(errno));
            exit(-1);
        }
    }

    if (munmap(shared_data1, sizeof(shared_data)) == -1)
    {
        perror("munmap failed");
        exit(-1);
    }

    if (close(fd) == -1)
    {
        perror("close() failed");
        exit(-1);
    }

    if (shm_unlink("/ex14shm") < 0)
    {
        perror("Error at unlink");
        exit(-1);
    }
    return 0;
}
