#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>
#include <errno.h>

#define THEATER_CAPACITY 300
#define NUM_SEM 4

static const int VIP = 0;
static const int SPECIAL = 1;
static const int NORMAL = 2;
static const int LUGARES = 3;

static const int ESPECTADORES_SAIR = 10; // Nr de espectadores a sair de modo a entrar os que estão em espera

static const char SEMAFOROS[NUM_SEM][20] = { "/vip", "/special", "/normal",
		"/lugaresVazios" };
static const int ESTADO_SEMAFORO[NUM_SEM] = { 0, 0, 0, 0 };

void unlinkSemaforos();

int main() {

	sem_t *sem[NUM_SEM];

	int i;
	for (i = 0; i < NUM_SEM; i++) {
		// VIP: i = 0 / Special: i = 1 / Normal: i = 2
		sem[i] = sem_open(SEMAFOROS[i], O_CREAT | O_EXCL, 0644,
				ESTADO_SEMAFORO[i]);
		if (sem[i] == SEM_FAILED) {
			perror("Erro no sem_open()!!\n");
			exit(1);
		}
	}

	int numVips = 0, numSpecial = 0, numNormal = 0;

	printf("Insira o numero de Espectadores VIP em espera: ");
	scanf("%d%*c", &numVips);
	for (i = 0; i < numVips; i++) {
		sem_post(sem[VIP]);
	}

	printf("Insira o numero de Espectadores Special em espera: ");
	scanf("%d%*c", &numSpecial);
	for (i = 0; i < numSpecial; i++) {
		sem_post(sem[SPECIAL]);
	}

	printf("Insira o numero de Espectadores Normal em espera: ");
	scanf("%d%*c", &numNormal);
	for (i = 0; i < numNormal; i++) {
		sem_post(sem[NORMAL]);
	}

	printf("------------------------------\n");
	printf("  - VIPs: %d\n", numVips);
	printf("  - Specials: %d\n", numSpecial);
	printf("  - Normals: %d\n", numNormal);
	printf("------------------------------\n\n");

	printf("------------------------------\n");
	printf("  Lugares Disponíveis: %d\n", THEATER_CAPACITY);
	printf("------------------------------\n");

	//enche a sala com pessoas aleatorias
	printf("\nA preencher a sala...\n");
	printf("Sala preenchida!\n\n");

	struct timespec ts;

	pid_t pid;
	pid = fork();
	if (pid > 0) {	//Uma pessoa sai da sala a cada 2 segundos
		for (i = 0; i < ESPECTADORES_SAIR; i++) {
			sem_post(sem[LUGARES]);
			printf("Um espectador saiu da sala!\n");
			sleep(2);
		}
	} else if (pid == 0) {
		// Sendo que saem espectadores a cada 2 segundos, ao fim de 5 segundos,
		// se não existir nenhum lugar livre, o processo termina porque já não
		// vão existir mais lugares livres.
		int vipEntry = 0, specialEntry = 0, normalEntry = 0, s, errorFlag = 0;

		while (sem_trywait(sem[VIP]) != -1 && errorFlag == 0) {
			if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
				perror("clock_gettime() failed!\n");
				return -1;
			}
			ts.tv_sec += 5;
			s = sem_timedwait(sem[LUGARES], &ts);
			if (s == -1 && errno == ETIMEDOUT) {
				errorFlag = 1;
				break;
			} else {
				printf("Entrou um Espectador VIP!\n\n");
				vipEntry++;
			}
		}
		while (sem_trywait(sem[SPECIAL]) != -1 && errorFlag == 0) {
			if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
				perror("clock_gettime() failed!\n");
				return -1;
			}
			ts.tv_sec += 5;
			s = sem_timedwait(sem[LUGARES], &ts);
			if (s == -1 && errno == ETIMEDOUT) {
				errorFlag = 1;
				break;
			} else {
				printf("Entrou um Espectador Special!\n\n");
				specialEntry++;
			}
		}
		while (sem_trywait(sem[NORMAL]) != -1 && errorFlag == 0) {
			if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
				perror("clock_gettime() failed!\n");
				return -1;
			}
			ts.tv_sec += 5;
			s = sem_timedwait(sem[LUGARES], &ts);
			if (s == -1 && errno == ETIMEDOUT) {
				errorFlag = 1;
				break;
			} else {
				printf("Entrou um Espectador Normal!\n\n");
				normalEntry++;
			}
		}

		int totalEntries = vipEntry + specialEntry + normalEntry;
		int total = numVips + numSpecial + numNormal;
		printf("--------------------------------------------\n");
		printf("  Das '%d' pessoas em espera entraram '%d'\n", total,
				totalEntries);
		printf("  Entraram '%d' VIPs em '%d'\n", vipEntry, numVips);
		printf("  Entraram '%d' Specials em '%d'\n", specialEntry, numSpecial);
		printf("  Entraram '%d' Normals em '%d'\n", normalEntry, numNormal);
		printf("---------------------------------------------\n\n");
		exit(EXIT_SUCCESS);
	}

	wait(NULL);
	unlinkSemaforos();

	return 0;
}

void unlinkSemaforos() {
	int i;
	for (i = 0; i < NUM_SEM; i++) {
		sem_unlink(SEMAFOROS[i]);
	}
}
