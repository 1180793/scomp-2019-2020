/*
 * ex15.c
 *
 *  Created on: 08/05/2020
 *      Author: isep
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <time.h>

#define SEM_NAME "/sem_ex15"
#define SEMS_NAME "/semS_ex15"

#define MAX_VISITORS 5
#define MIN_WATCH_TIME 10
#define N_CHILDS 23

#define SESSIONS 5
#define PREPARATION_TIME 2
#define FIRST_HALF 5
#define SECOND_HALF 15
#define SHOW_DURATION FIRST_HALF + SECOND_HALF

int main(){

	int i;
	int r;
	int ready;
	pid_t pid[N_CHILDS];
	sem_t *sem;
	sem_t *semShow;
	srand((unsigned) time(NULL));

	sem = sem_open(SEM_NAME, O_CREAT, 0644, MAX_VISITORS);
	if ((sem == SEM_FAILED)) {
		perror("sem_open error...");
		exit(1);
	}

	semShow = sem_open(SEMS_NAME, O_CREAT, 0644, 0);
	if ((semShow == SEM_FAILED)) {
		perror("sem_open error...");
		exit(1);
	}


	for( i = 0; i < N_CHILDS; i++){

		pid[i] = fork();

		if(pid[i] == 0){

			pid[i] = getpid();

			//Visitor Queue
			sem_wait(sem);
			//Show waiting area
			do{
				sem_getvalue(semShow, &ready);
			}while(!ready);
			sleep(1);

			printf("Process number %d has entered the show.\n", pid[i]);

			int watch_time = (rand() % (SHOW_DURATION - MIN_WATCH_TIME + 1)) + MIN_WATCH_TIME;

			sleep(watch_time);

			printf("Process number %d has left the show.\n", pid[i]);
			sem_post(sem);

			return 1;
		}

	}

	//Manages the show room
	for( i = 0; i < SESSIONS; i ++){

		sem_post(semShow);
		printf("\nThe show has stared.\n\n");

		sleep(FIRST_HALF);
		sem_wait(semShow);
		printf("\n");

		sleep(SECOND_HALF);

		printf("\nThe show is over.\n");

		if( i < SESSIONS - 1 )
		printf("\nPreparing for the next show.\n");
		sleep(PREPARATION_TIME);

	}

	printf("\n");

	r = sem_unlink(SEM_NAME);
	if(r == -1){
	perror("sem_unlink error...");
	exit(EXIT_FAILURE);
	}

	r = sem_unlink(SEMS_NAME);
	if(r == -1){
	perror("sem_unlink error...");
	exit(EXIT_FAILURE);
	}

}
