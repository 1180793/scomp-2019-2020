/*
 * ex10.c
 *
 *  Created on: 05/05/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdbool.h>
#include "header.h"


void consult(int id){

	int i;
	int fd;
	int ft;
	int r;
	int data_size = sizeof(records) * NUM_RECORDS;
	records* addr;
	sem_t *sem;

	fd = shm_open(SHM_NAME, O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	if(fd == -1) {
		perror("shm_open error...");
		exit(EXIT_FAILURE);
	}

	ft = ftruncate (fd, data_size);
	if(ft == -1){
		perror("ftruncate error...");
		exit(EXIT_FAILURE);
	}

	addr = (records*) mmap(NULL, data_size,PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if( addr == MAP_FAILED){
		perror("mmap error...");
		exit(EXIT_FAILURE);
	}

	sem = sem_open(SEM_NAME, O_CREAT, 0644, 1);
	if ((sem == SEM_FAILED)) {
		perror("sem_open error...");
		exit(1);
	}

	sem_wait(sem);

	for(i = 0; i < NUM_RECORDS; i++){

		if( addr[i].Number == id && addr[i].isActive == true){
			printf("User id: %d\n", addr[i].Number);
			printf("User Name: %s\n", addr[i].Name);
			printf("User Address: %s\n", addr[i].Address);
			break;
		}
	}

	if( i == NUM_RECORDS)
		printf("User not found...");

	sem_post(sem);

	r = munmap(addr, data_size);
	if(r == -1){
		perror("munmap error...");
		exit(EXIT_FAILURE);
	}

}

void insert(int id, char name[], char add[]){

	int i;
	int fd;
	int ft;
	int r;
	int data_size = sizeof(records) * NUM_RECORDS;
	records* addr;
	sem_t *sem;

	fd = shm_open(SHM_NAME, O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	if(fd == -1) {
		perror("shm_open error...");
		exit(EXIT_FAILURE);
	}

	ft = ftruncate (fd, data_size);
	if(ft == -1){
		perror("ftruncate error...");
		exit(EXIT_FAILURE);
	}

	addr = (records*) mmap(NULL, data_size,PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if( addr == MAP_FAILED){
		perror("mmap error...");
		exit(EXIT_FAILURE);
	}

	sem = sem_open(SEM_NAME, O_CREAT, 0644, 1);
	if ((sem == SEM_FAILED)) {
		perror("sem_open error...");
		exit(1);
	}

	sem_wait(sem);

	for(i = 0; i < NUM_RECORDS; i++){

		if( addr[i].isActive == false){
			break;
		}

		if( addr[i].Number == id){
			i = -1;
			break;
		}
	}

	if( i == NUM_RECORDS)
		printf("Max capacity reached...");
	else if( i == -1 ){
		printf("\nUser number already exists...\n");
	}else{
		addr[i].Number = id;
		strcpy(addr[i].Name, name);
		strcpy(addr[i].Address, add);
		addr[i].isActive = true;
		printf("\nUser data added successfully.\n");
	}

	sem_post(sem);

	r = munmap(addr, data_size);
	if(r == -1){
		perror("munmap error...");
		exit(EXIT_FAILURE);
	}

}

void consultALL(){

	int i;
	int fd;
	int ft;
	int r;
	int data_size = sizeof(records) * NUM_RECORDS;
	records* addr;
	sem_t *sem;

	fd = shm_open(SHM_NAME, O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	if(fd == -1) {
		perror("shm_open error...");
		exit(EXIT_FAILURE);
	}

	ft = ftruncate (fd, data_size);
	if(ft == -1){
		perror("ftruncate error...");
		exit(EXIT_FAILURE);
	}

	addr = (records*) mmap(NULL, data_size,PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if( addr == MAP_FAILED){
		perror("mmap error...");
		exit(EXIT_FAILURE);
	}

	sem = sem_open(SEM_NAME, O_CREAT, 0644, 1);
	if ((sem == SEM_FAILED)) {
		perror("sem_open error...");
		exit(1);
	}

	sem_wait(sem);

	for(i = 0; i < NUM_RECORDS; i++){

		if( addr[i].isActive == true){
			printf("\n");
			printf("User id: %d\n", addr[i].Number);
			printf("User Name: %s\n", addr[i].Name);
			printf("User Address: %s\n", addr[i].Address);
		}
	}

	sem_post(sem);

	r = munmap(addr, data_size);
	if(r == -1){
		perror("munmap error...");
		exit(EXIT_FAILURE);
	}

}





