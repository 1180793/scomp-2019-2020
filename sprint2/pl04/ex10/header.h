/*
 * header.h
 *
 *  Created on: 05/05/2020
 *      Author: isep
 */

#ifndef HEADER_H_
#define HEADER_H_

#define NUM_RECORDS 100

#define SHM_NAME "/shm_ex10"
#define SEM_NAME "/sem_ex10"

typedef struct{
	int Number;
	char Name[50];
	char Address[100];
	bool isActive;
}records;

void consult(int id);
void insert(int id, char name[], char add[]);
void consultALL();


#endif /* HEADER_H_ */
