/*
 * shutdown.c
 *
 *  Created on: 07/05/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdbool.h>
#include "header.h"

int main(){

	int r;

	r = shm_unlink(SHM_NAME);
	if(r == -1){
	perror("shm_unlink error...");
	exit(EXIT_FAILURE);
	}

	r = sem_unlink(SEM_NAME);
	if(r == -1){
	perror("sem_unlink error...");
	exit(EXIT_FAILURE);
	}

	printf("Shutting down...\n");
}
