/*
 * startup.c
 *
 *  Created on: 07/05/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdbool.h>
#include "header.h"

int main(){

		int i;
		int fd;
		int ft;
		int r;
		records *addr;
		int data_size = sizeof(records) * NUM_RECORDS;

		fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
		if(fd == -1) {
				perror("shm_open error...");
				exit(EXIT_FAILURE);
		}

		ft = ftruncate (fd, data_size);
		if(ft == -1){
			perror("ftruncate error...");
			exit(EXIT_FAILURE);
		}

		addr = (records*) mmap(NULL, data_size,PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
		if( addr == MAP_FAILED){
			perror("mmap error...");
			exit(EXIT_FAILURE);
		}

		//Sets every record to non active
		for( i = 0; i < NUM_RECORDS; i++){

			addr[i].isActive = false;
		}

		r = munmap(addr, data_size);
		if(r == -1){
			perror("munmap error...");
			exit(EXIT_FAILURE);
		}

		printf("The program has been started...\n");

		return 0;
}
