/*
 * main.c
 *
 *  Created on: 05/05/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdbool.h>
#include "header.h"

int main(){

	int check = 0;
	int id = 0;
	int number = 0;
	char name[50];
	char add[100];

	do{

		printf("\nChoose an option:\n1 - Consult\n2 - Insert\n3 - Consult_ALL\n0 - Exit\n:");
		scanf("%d", &check);

		switch(check){
		case 1:

			printf("\nUser identification number: ");
			scanf("%d", &id);

			consult(id);

			break;
		case 2:

			printf("User identification number: ");
			scanf("%d", &number);

			printf("User name: ");
			scanf("%s", name);

			printf("User address: ");
			scanf("%s", add);

			insert(number, name, add);

			break;
		case 3:

			consultALL();
			break;
		case 0:
			break;
		}


	}while(check);

	return 0;
}
