#include <errno.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char *agrv[]) {
    const int nrMsg = 15;
    const int nrSemaforos = 2;
    const char nomeSemaforos[2][80] = {"semPai", "semFilho"};
    const int valoresSemaforo[2] = {0, 1};
    const int semFather = 0;
    const int semChild = 1;
    sem_t *semaforos[nrSemaforos];

    int i;
    for (i = 0; i < nrSemaforos; i++) {
        if ((semaforos[i] = sem_open(nomeSemaforos[i], O_CREAT | O_EXCL, 0644, valoresSemaforo[i])) == SEM_FAILED) {
            perror("Error at sem_open()!\n");
            exit(-1);
        }
    }

    pid_t pid = fork();
    
    if (pid > 0) {
        for (i = 0; i < nrMsg / 2; i++) {
            if (sem_wait(semaforos[semFather]) == SEM_FAILED) {
                perror("Error at sem_wait()!");
                exit(-1);
            }
            printf("I'm the father!\n");
            if (sem_post(semaforos[semChild]) == SEM_FAILED) {
                perror("Error at sem_post()!");
             
                exit(-1);
            }
        }
    }

    if (pid == 0) {
        for (i = 0; i < (nrMsg / 2) + 1; i++) {
            if (sem_wait(semaforos[semChild]) == SEM_FAILED) {
                perror("Error at sem_wait()!");
                exit(EXIT_FAILURE);
            }
            printf("I'm the child!\n");
            if (sem_post(semaforos[semFather]) == SEM_FAILED) {
                perror("Error at sem_post()!");
                exit(EXIT_FAILURE);
            }
        }
        exit(EXIT_SUCCESS);
    }

    for (i = 0; i < nrSemaforos; i++) {
        if (sem_close(semaforos[i]) == SEM_FAILED) {
            perror("Error at sem_close()!\n");
            exit(-1);
        }
    }
    for (i = 0; i < nrSemaforos; i++) {
        if (sem_unlink(nomeSemaforos[i]) == SEM_FAILED) {
            perror("Error at sem_unlink()!\n");
            exit(-1);
        }
    }

    return 0;
}
