/*
 * header.h
 *
 *  Created on: 09/05/2020
 *      Author: isep
 */

#ifndef HEADER_H_
#define HEADER_H_

#define NUM_STR 50
#define STR_SIZE 80
#define SHM_NAME "/shm_ex03"
#define SEM_NAME "/sem_ex03"


typedef struct{
	char s[NUM_STR][STR_SIZE];
	int index;
}str;

struct timespec ts;

#endif /* HEADER_H_ */
