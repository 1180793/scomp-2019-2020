/*
 * ex04.c
 *
 *  Created on: 09/05/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <time.h>
#include <errno.h>
#include "header.h"

int main(){

	int i = 0;
	int s;
	int fd;
	int ft;
	int r;
	int data_size = sizeof(str);
	str* addr;
	pid_t pid;
	sem_t *sem;

	fd = shm_open(SHM_NAME, O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	if(fd == -1) {
			perror("shm_open error...");
			exit(EXIT_FAILURE);
	}

	ft = ftruncate (fd, data_size);
	if(ft == -1){
		perror("ftruncate error...");
		exit(EXIT_FAILURE);
	}

	addr = (str*) mmap(NULL, data_size,PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if( addr == MAP_FAILED){
		perror("mmap error...");
		exit(EXIT_FAILURE);
	}

	sem = sem_open(SEM_NAME, O_CREAT, 0644, 1);
	if ((sem == SEM_FAILED)) {
		perror("sem_open error...");
		exit(1);
	 }

	if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
	{
		perror("click_gettime error...");
		exit(1);
	}

	//12 second wait time
	ts.tv_sec += 12;

	while ((s = sem_timedwait(sem, &ts)) == -1 && errno == EINTR)
	        continue;

	if (s == -1) {
	     if (errno == ETIMEDOUT)
	         printf("Connection timed out...");
	     else
	         perror("sem_timedwait error...");
	     exit(1);
	}

	pid = getpid();

	char buffer[STR_SIZE];

	snprintf(buffer, sizeof(buffer), "I’m the Father - with PID %ld", (long) pid);

	i = addr->index;

	strcpy(addr->s[i], buffer);

	printf("%s\n", addr->s[i]);

	addr->index++;

	int sleepTimer = random() % 5 + 1;

	sleep(sleepTimer);

	sem_post(sem);

	r = munmap(addr, data_size);
	if(r == -1){
		perror("munmap error...");
		exit(EXIT_FAILURE);
	}

	//Unlinks after 10 processes
	if( i+1 == 10 ){
		r = shm_unlink(SHM_NAME);
		if(r == -1){
			perror("shm_unlink error...");
			exit(EXIT_FAILURE);
		}
		r = sem_unlink(SEM_NAME);
		if(r == -1){
		perror("sem_unlink error...");
		exit(EXIT_FAILURE);
		}
	}

	return 0;

}

