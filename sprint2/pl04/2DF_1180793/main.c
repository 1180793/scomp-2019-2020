#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>
#include <errno.h>

#define SHM_NAME "/shm_1180793"
#define NUM_CHILD 5
#define NUM_SEM 1

static const char SEMAFOROS[20] = { "/sem_1180793" };
static const char ALGORITMOS[5][3] = { "A1", "A2", "A3", "A4", "A5" };
static const int ESTADO_SEMAFORO = 1;

int forkChild(int n);
int randomNumber(int min, int max);

typedef struct {
	char nome[3];
	int tempoExec;
} algoritmo;

int main() {

	sem_t *sem;

	int i;
	sem = sem_open(SEMAFOROS, O_CREAT | O_EXCL, 0644, ESTADO_SEMAFORO);
	if (sem == SEM_FAILED) {
		perror("sem_open() failed!\n");
		exit(1);
	}

	// Shared Memory
	// 1. Cria e abre a shared memory
	int fd = shm_open(SHM_NAME, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
	if (fd == -1) {
		printf("Error at shm_open()!\n");
		exit(EXIT_FAILURE);
	}

	// 2. Tamanho das 5 structs do tipo algoritmo
	int sizeTotal = 5 * sizeof(algoritmo);
	ftruncate(fd, sizeTotal);

	// 3. Apontador do tipo algoritmo
	algoritmo *sharedData;
	sharedData = (algoritmo *) mmap(NULL, sizeTotal, PROT_READ | PROT_WRITE,
	MAP_SHARED, fd, 0);

	int id = forkChild(5);
	if (id != -1) {
		//Retira 1 ao semaforo, se possivel, para impedir que outro processo possa executar
		sem_wait(sem);

		int number = randomNumber(0, 10);
		sleep(number);

		//Escreve na shared memory
		strcpy(sharedData[id].nome, ALGORITMOS[id]);
		sharedData[id].tempoExec = number;

		printf("Algoritmo %d Terminado - Tempo de Execução: %d secs\n", id + 1,
				number);

		//Aumenta o semaforo ao acabar, para que outro processo possa executar
		sem_post(sem);
		exit(EXIT_SUCCESS);
	}

	//Processo pai espera pelos filhos
	for (i = 0; i < NUM_CHILD; i++) {
		wait(NULL);
	}

	//Calcula o menor tempo
	int idMelhorTempo = 0;
	int melhorTempo = sharedData[0].tempoExec;
	for (i = 1; i < NUM_CHILD; i++) {
		if (sharedData[i].tempoExec < melhorTempo) {
			melhorTempo = sharedData[i].tempoExec;
			idMelhorTempo = i;
		}
	}

	printf(
			"\nO melhor tempo de execução é do Algoritmo '%s' com um tempo de execução de %d segundos.\n",
			sharedData[idMelhorTempo].nome,
			sharedData[idMelhorTempo].tempoExec);

	// Undo mapping
	if (munmap((void *) sharedData, sizeTotal) < 0) {
		printf("munmap() failed!\n");
		exit(EXIT_FAILURE);
	}

	// Close file descriptor
	if (close(fd) < 0) {
		printf("close() failed!\n");
		exit(EXIT_FAILURE);
	}

	// Shared memory unlink
	if (shm_unlink(SHM_NAME) < 0) {
		printf("shm_unlink() failed!\n");
		exit(EXIT_FAILURE);
	}

	if (sem_close(sem) == -1) {
		perror("Error at sem_close()!\n");
		exit(EXIT_FAILURE);
	}
	if (sem_unlink(SEMAFOROS) == -1) {
		perror("Error at sem_unlink()!\n");
		printf("Error: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	return 0;
}

int forkChild(int n) {
	pid_t pid;
	int i;
	for (i = 0; i < n; i++) {
		pid = fork();
		if (pid < 0) {
			return -1;
		} else if (pid == 0) {
			return i;
		}
	}
	return -1;
}

int randomNumber(int min, int max) {
	srand(time(NULL) * getpid() << 16);
	int randomNumber = (rand() % max) + min;
	return randomNumber;
}
