#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fcntl.h>
#include <semaphore.h>

#define SHM_NAME "/shm_ex11"
#define NUM_PORTAS 3
#define MAX_PASSAGEIROS 200
#define NUM_SEMAFOROS 4
#define TEST_TIME 50
#define SEM_PASSAGEIROS 3

static const int mill = 1000 * TEST_TIME;

void movimentos(sem_t *sem[NUM_SEMAFOROS], int* shared_memory, int nPorta,
		clock_t startTime) {
	//semente para numero aleatorio
	srand(time(NULL));
	while (clock() < startTime + mill) {
		int random = (rand() % 2) + 1;
		// 1 - Entra Passageiro
		if (random == 1) {
			if (sem_wait(sem[nPorta]) == -1) {
				perror("sem_wait() failed!");
				exit(EXIT_FAILURE);
			}
			if (sem_wait(sem[SEM_PASSAGEIROS]) == -1) {
				perror("sem_wait() failed!");
				exit(EXIT_FAILURE);
			}

			if (*(shared_memory) < MAX_PASSAGEIROS) {
				*(shared_memory) = *(shared_memory) + 1;
				printf(
						"Entrou um passageiro na Porta %d\nPassageiros Atuais: %d\n\n",
						nPorta + 1, *(shared_memory));
			} else {
				printf("\nCapacidade Máxima atingida!\n\n");
			}

			if (sem_post(sem[SEM_PASSAGEIROS]) == -1) {
				perror("sem_post() failed!");
				exit(EXIT_FAILURE);
			}
			if (sem_post(sem[nPorta]) == -1) {
				perror("sem_post() failed!");
				exit(EXIT_FAILURE);
			}
		} else {
			if (sem_wait(sem[nPorta]) == -1) {
				perror("sem_wait() failed!");
				exit(EXIT_FAILURE);
			}
			if (sem_wait(sem[SEM_PASSAGEIROS]) == -1) {
				perror("sem_wait() failed!");
				exit(EXIT_FAILURE);
			}

			if (*(shared_memory) == 0) {
				printf("\nNão existem passageiros para sair do comboio!\n\n");
			} else {
				*(shared_memory) = *(shared_memory) - 1;
				printf(
						"Saiu um passageiro pela Porta %d\nPassageiros Atuais: %d\n\n",
						nPorta + 1, *(shared_memory));
			}

			if (sem_post(sem[SEM_PASSAGEIROS]) == -1) {
				perror("sem_post() failed!");
				exit(EXIT_FAILURE);
			}

			if (sem_post(sem[nPorta]) == -1) {
				perror("sem_post() failed!");
				exit(EXIT_FAILURE);
			}
		}
	}
	printf("Fim Porta %d\n\n", nPorta + 1);
	exit(EXIT_SUCCESS);
}

int main(void) {
	const char SEMAFOROS[NUM_SEMAFOROS][50] = { "PORTA_01", "PORTA_02",
			"PORTA_03", "PASSAGEIROS" };
	const int SEMAFOROS_INITIAL_VALUE[NUM_SEMAFOROS] = { 1, 1, 1, 1 };
	sem_t *sem[NUM_SEMAFOROS];

	// Shared memory
	int fd = shm_open(SHM_NAME, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
	if (fd == -1) {
		perror("shm_open() failed!");
		exit(EXIT_FAILURE);
	}

	int truncate = ftruncate(fd, sizeof(int));
	if (truncate == -1) {
		perror("ftruncate() failed!");
		exit(EXIT_FAILURE);
	}

	int* shared_memory;
	shared_memory = (int*) mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE,
	MAP_SHARED, fd, 0);

	*(shared_memory) = 0;

	int i;
	for (i = 0; i < NUM_SEMAFOROS; i++) {
		if ((sem[i] = sem_open(SEMAFOROS[i], O_CREAT | O_EXCL, 0644,
				SEMAFOROS_INITIAL_VALUE[i])) == SEM_FAILED) {
			perror("sem_open() failed!\n");
			exit(EXIT_FAILURE);
		}
	}

	// tempo inicial
	clock_t startTime = clock();

	printf("\nPassageiros Iniciais: %d\n\n", *(shared_memory));

	pid_t pid;
	for (i = 0; i < NUM_PORTAS; i++) {
		pid = fork();
		if (pid == -1) {
			printf("Fork failed!!\n");
			exit(EXIT_FAILURE);
		} else if (pid == 0) {    //processo filho
			printf("Filho %d\n\n", i);
			movimentos(sem, shared_memory, i, startTime);
		}
	}

	for (i = 0; i < NUM_PORTAS; i++) { //espera que os processos acabem
		wait(NULL);
	}

	for (i = 0; i < NUM_SEMAFOROS; i++) { //fecha os semaforos
		if (sem_unlink(SEMAFOROS[i]) == -1) {
			perror("Sem_unlink failed!\n");
			exit(EXIT_FAILURE);
		}
	}

	munmap(shared_memory, sizeof(shared_memory));
	close(fd);
	shm_unlink(SHM_NAME); //fecha a memoria partilhada

	return 0;
}
