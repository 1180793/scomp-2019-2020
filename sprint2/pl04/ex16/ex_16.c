/*
 * ex_16.c
 *
 *  Created on: 07/05/2020
 *      Author: isep
 */

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>

#define CARROS_ESTE 5
#define CARROS_OESTE 4

typedef struct {
	int este;
	int oeste;
	int esteFinal;
	int oesteFinal;
	int tempo;
}lados;

void este(sem_t *sem, lados *shared_data);
void oeste(sem_t *sem, lados *shared_data);

int main(){
	lados *shared_data;

	//CriaÃ§Ã£o da memÃ³ria partilhada
	int fd;

	fd = shm_open("memoria", O_CREAT|O_RDWR, S_IRWXU|S_IRWXG);

	//Erro na criaÃ§Ã£o da memÃ³ria partilhada
	if(fd == -1){
		perror("Erro na criaÃ§Ã£o da memÃ³ria partilhada(shm_open)\n");
		exit(EXIT_FAILURE);
	}

	//Definir o tamanho da memÃ³ria partilhada
	int erro_memoria, data_size = sizeof(lados);

	erro_memoria = ftruncate(fd, data_size);

	//Erro a definir o tamanho da memÃ³ria partilhado
	if(erro_memoria == -1){
		perror("Erro a definir o tamanho da memÃ³ria partilhada(f_truncate)\n");
		exit(EXIT_FAILURE);
	}

	//Mapear a memoria partilhada ao espaco de enderecamento do processo
	shared_data = mmap(NULL,data_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);

	//Erro a mapear a memÃ³ria partilhada
    if (shared_data == NULL) {
        perror("Erro a mapear a memÃ³ria partilhada\n");
        exit(EXIT_FAILURE);
    }

    //CriaÃ§Ã£o dos semÃ¡foros
	sem_t *sem1;

	//SemÃ¡foro 1
	if((sem1 = sem_open("SemÃ¡foro1",O_CREAT|O_EXCL,0644,1))== SEM_FAILED){
		perror("Erro na criaÃ§Ã£o do semÃ¡foro 1");
		exit(EXIT_FAILURE);
	}


    shared_data->este=CARROS_ESTE;
    shared_data->oeste=CARROS_OESTE;

    // Se o argumento for este
	if(shared_data->este > 0) {
		este(sem1, shared_data);
	}
	//Tem o mesmo comportamento que o codigo de cima, so que feito no sentido oeste->este
	if(shared_data->oeste >0) {
		oeste(sem1, shared_data);
	}
	printf("JÃ¡ nÃ£o hÃ¡ mais carros.\n");
	printf("Tempo total %d segundos.\n", shared_data->tempo*30);

	//RemoÃ§Ã£o do sem1
	int erro_semaforos;

	//SemÃ¡foro 1
	erro_semaforos = sem_unlink("SemÃ¡foro1");

	//Erro a remover o sem1
	if(erro_semaforos == -1){
		perror("Unlink do semÃ¡foro 1 falhou");
		exit(EXIT_FAILURE);
	}



	return 0;
}

void este(sem_t *sem, lados *shared_data){

	while(shared_data->este > 0) {
		//Incrementa um carro de este para oeste
		sem_wait(sem);
		shared_data->este--;

		//Decrementa um carro de este para oeste
		shared_data->oesteFinal++;


		printf("Carro no sentido Este -> Oeste.\n");
		sleep(3);
		shared_data->tempo= 3 + shared_data->tempo;

		sem_post(sem);
	}

}

void oeste(sem_t *sem, lados *shared_data){

	while(shared_data->oeste > 0) {
		//Incrementa um carro de este para oeste
		sem_wait(sem);
		shared_data->oeste--;

		//Decrementa um carro de este para oeste
		shared_data->esteFinal++;


		printf("Carro no sentido Oeste -> Este.\n");
		sleep(3);
		shared_data->tempo= 3 + shared_data->tempo;

		sem_post(sem);
	}
}

