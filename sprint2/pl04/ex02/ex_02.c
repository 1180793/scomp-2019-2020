#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include "ex_01.h"

static const char direcs[2][15] = {'Numbers.txt','Output.txt'};
static const char SEMAFOROS[8][20] = {"/1","/2","/3","/4,","/5","/6","/7","/8"}; //nome de cada semaforo
static const int ESTADO_SEMAFORO[8] = {1,0,0,0,0,0,0,0}; //estado inicial de cada semaforo


main(void){

pid_t pids[NUMBER_CHILDREN];
int max_nums = NUMBER_CHILDREN*PROCESSES_PER_CHILDREN;
int i;

sem_t *leitura = sem_open("/leitura", O_CREAT|O_EXCL,0644,1);

	
if(leitura == SEM_FAILED){
		perror("Sem_open() failed!!");
		exit(EXIT_FAILURE);
}
	
sem_t *escrita;

escrita = sem_open("/escrita", O_CREAT|O_EXCL,0644,1);
	
if(escrita == SEM_FAILED){
	perror("Sem_open() failed!!");
	exit(EXIT_FAILURE);
}

sem_t *sem[NUMBER_CHILDREN];

	int j;

	for(j=0;j<NUMBER_CHILDREN;j++){ //criar 8 semaforos

		sem[j]	= sem_open(SEMAFOROS[j],O_CREAT,0644,ESTADO_SEMAFORO[j]);
	
		if (sem[j] == SEM_FAILED) {
			perror("Erro no sem_open()!!\n");
			exit(1);
		}

	}


FILE *fp; //file numbers.txt
	
	if((fp = fopen(direcs[LEITURA], "r")) == NULL){ //verifica se ocorreu com sucesso a leitura do numbers.txt
		printf("File Error!!");
		exit(EXIT_FAILURE);
	}

for(int i = 0; i<NUMBERS_PER_PROCESS*NUMBER_CHILDREN; i++ ){ //processo pai imprime todos os valores do numbers.txt
	fprintf(fp,"%d\n", i+1);
}

fclose(fp);

int nChildren = 0;

for(i=0; i< NUMBER_CHILDREN; i++){
		
		pid[i]= fork(); //>0 processo pai, == 0 processo filho, == -1 erro
		
		if(pid[i] == -1){
			perror("Error creating child!");
			return EXIT_FAILURE;
		}
	
		if(pid[i] == 0){
		
			nChildren = i + 1;
			son(nChildren,leitura,escrita);
	
		}
		
	}
	
	for(i=0; i< NUMBER_CHILDREN; i++){	
		wait(NULL); //will block parent process until any of its children has finished
	}
	
	sem_unlink("/leitura");	
	sem_unlink("/escrita");	

	return 0;
}

void son(int nChildren, sem_t *leitura, sem_t *escrita){
	
	int arrayValoresLidos[NUMBERS_PER_PROCESS];
	
	
	sem_wait(sem[nChildren-1]);	//decrementa semaphor corresponding to current child

	sem_wait(leitura); //read Numbers.txt

		FILE *fp; //file numbers.txt
	
		int line_num = 0; 
		int cont = 0;
	
		if((fp = fopen(direcs[LEITURA], "r")) == NULL){ //verifica se ocorreu com sucesso a abertura do ficheiro de leitura
			printf("File Error!!");
			exit(EXIT_FAILURE);
		}
		
		int lowLimit = NUMBER_RANGE*(numChilds-1);
		int upLimit = NUMBER_RANGE*numChilds;
	
		printf("----------------------\n");
		printf("filho numero: %d\n",numChilds);
		printf("lowLimit-> %d | upLimit > %d \n",lowLimit,upLimit);
		
		int desnecessario=0;
		
		for(line_num=0;line_num<max_nums;line_num++){
			
			if( (line_num >= lowLimit) && (line_num < upLimit)){			
				fscanf(fp,"%d", &arrayValoresLidos[cont]);
				printf("arrayRead-->%d\n",arrayValoresLidos[cont]);
				cont++;
				
			}else{
				fscanf(fp,"%d ", &desnecessario);
			}
			
		}
		
		printf("cont--> %d\n", cont);
		
		fclose(fp);
	
	sem_post(leitura); //incrementa semaforo de eleitura
	
	//escrita no novo ficheiro
	sem_wait(escrita);
	
		FILE *fp1; //file output
	
		if((fp1 = fopen(FILES[ESCRITA], "a")) == NULL){ //verifica se ocorreu com sucesso a abertura do ficheiro de escrita
			printf("File Error!!");
			exit(EXIT_FAILURE);
		}
		
		int j;
	
		for(j=0; j<max_nums; j++){
		
			fprintf(fp1,"%d\n", arrayValoresLidos[j]);
		
		}
	
		fclose(fp1);

	sem_post(escrita);
	
	exit(1);
		
}
