/*
 * ex_90.c
 *
 *  Created on: 07/05/2020
 *      Author: isep
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fcntl.h>
#include <semaphore.h>

#define SHM_NAME "/shm_ex09"

static const int BEER_CHIPS = 0;
static const int BARREIRA = 1;
static const int NUM_SEM = 2;
static const char SEMAFOROS[2][20] = {"/sem_beer_and_chips","/sem_barreira"};
static const int ESTADO_SEMAFORO[2] = {1,0};

typedef struct{
	int beerChips;
}shmem;

void buy_chips(){
	printf("The chips are acquired !!\n");
}

void buy_beer(){
	printf("The beer are acquired !!\n");
}

void eat_and_drink(){
	printf("Both the beer and chips are acquired!!\n");
}
int main (void){

	int i;

	sem_t *sem[NUM_SEM];

	shmem *shared_data;

	int data_size = sizeof(shmem);

	int fd1;

	int trunc = 0;

	//Obter file descriptor
	fd1 = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);

	if(fd1 == -1) {
		perror("Shm_open Error!!");
		exit(-1);
	}

	//Define o tamanho da memÃ³ria partilhada
	trunc = ftruncate (fd1, data_size);

	if(trunc == -1){
		perror("Ftruncate Error!!");
		exit(-1);
	}

	shared_data = (shmem*) mmap(NULL, data_size ,PROT_READ|PROT_WRITE, MAP_SHARED, fd1, 0);

	if(shared_data == MAP_FAILED){
		perror("Mmap Error!!");
		exit(-1);
	}

	for(i=0;i<NUM_SEM;i++){
		sem[i]	= sem_open(SEMAFOROS[i],O_CREAT|O_EXCL,0644,ESTADO_SEMAFORO[i]);

		if (sem[i] == SEM_FAILED) {
			perror("Erro no sem_open()!!");
			exit(1);
		}
	}


	pid_t pid;



	pid= fork();

	if(pid== -1){
		printf("Fork failed!!\n");
		exit(EXIT_FAILURE);

	}else if(pid == 0){    //processo filho

		sem_wait(sem[BEER_CHIPS]);

		buy_chips();

		shared_data->beerChips++;

		sem_post(sem[BEER_CHIPS]);

		if(shared_data->beerChips == 2){

			sem_post(sem[BARREIRA]);
		}

		sem_wait(sem[BARREIRA]);

		eat_and_drink();

		sem_post(sem[BARREIRA]);

		exit(EXIT_SUCCESS);

	}

	sem_wait(sem[BEER_CHIPS]);

	buy_beer();

	shared_data->beerChips++;

	sem_post(sem[BEER_CHIPS]);

	if(shared_data->beerChips == 2){

		sem_post(sem[BARREIRA]);
	}

	sem_wait(sem[BARREIRA]);

	eat_and_drink();

	sem_post(sem[BARREIRA]);


	wait(NULL);

	int error = 0;

	error = munmap(shared_data,data_size);

	//Tratamento de erros
	if(error == -1) {
		perror("Munmap Error!!");
		exit(-1);
	}

	for(i=0;i<NUM_SEM;i++){
		sem_unlink(SEMAFOROS[i]);
	}

	return 0;
}


