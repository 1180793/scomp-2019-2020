/*
 * ex04.c
 *
 *  Created on: 27/03/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#define SIZE 101

int main(){

	int i;
	int fd;
	int ft;
	int data_size = sizeof(char) * SIZE;

	char* addr;

	pid_t pid;

	fd = shm_open("/shm_ex04", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	if(fd == -1) {
		perror("shm_open error...");
		exit(EXIT_FAILURE);
	}

	ft = ftruncate (fd, data_size);

	if(ft == -1){
		perror("ftruncate error...");
		exit(EXIT_FAILURE);
	}

	addr = (char*) mmap(NULL, data_size,PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	addr[SIZE + 1] = 0;

	pid = fork();

	//Child process
	if( pid == 0){

		while(!addr[SIZE + 1]);

		int total = 0;
		for( i = 0; i < SIZE; i++){
			printf("%c - index: %d\n", addr[i], i);
			total += addr[i];
		}

		printf("Average of the 100 ASCII codes: %d\n", total/100);

		return 0;
	}else if(pid == -1){
			exit(-1);
	}

	//Parent Process
	for( i = 0; i < SIZE; i++){
		addr[i] = random() % (90 + 1 - 65) + 65;
		printf("%c - index: %d\n", addr[i], i);
	}
	addr[SIZE + 1] = 1; //sets the flag to 1

	wait(NULL);

	return 0;
}
