#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/time.h>
#include "includes.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

static const int LEITURA = 0;
static const int ESCRITA = 1;

typedef struct s2 {
	int i;
	char texto[20];
}s2;

struct s2 vec[100000];

int main(void) {

	pid_t pid;
	int fd[2];

	if (pipe(fd) == -1) {
		perror("Criacao do pipe falhou");
		exit(-1);
	}

	pid_t pidPai = getpid();
	pid = fork();

	if (pid == -1) {
		perror("Erro na criaçao do processo filho");
		exit(1);
	}
	if (pid > 0) {
		int j;
		for (j = 0; j < ARRAY_SIZE; j++) {
			(vec + j)->i = rand() % 200 + 1;
			strncpy((vec + j)->texto, "ISEP – SCOMP 2020", 20);
		}

		clock_t beforePipes = clock();
		for (j = 0; j < ARRAY_SIZE; j++) {
			write(fd[ESCRITA], &vec + j, sizeof(vec));
		}
		close(fd[ESCRITA]);

		clock_t differencePipes = clock() - beforePipes;
		int msecPipes = differencePipes * 1000 / CLOCKS_PER_SEC;
		printf("\nPipe Write Time Taken: %d Ms", msecPipes);

	} else {
		int status, j;
		waitpid(pidPai, &status, 0);

		close(fd[ESCRITA]); // fecha a escrita do pipe que nao vai ser necessaria

		clock_t beforePipes = clock();
		for (j = 0; j < ARRAY_SIZE; j++) {
			read(fd[LEITURA], vec + j, sizeof(struct s2));
		}
		close(fd[LEITURA]);

		clock_t differencePipes = clock() - beforePipes;
		int msecPipes = differencePipes * 1000 / CLOCKS_PER_SEC;
		printf("\nPipe Read Time Taken: %d Ms", msecPipes);
		exit(1);
	}
	//--------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------

	s2 svec[ARRAY_SIZE];	//declares an array of s2 structs

	int fd2, data_size = sizeof(svec);

	//Declares shared_data as a pointer to array of s2 structs

	fd2 = shm_open("/ex03", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);

	if (fd2 == -1) {
		perror("There was an error opening the shared memory");
		exit(1);
	}

	int test = ftruncate(fd2, data_size);

	if (test == -1) {
		perror("There was an error defining the size of the shared memory area");
		exit(1);
	}

	//creates a pointer to the shared memory
	s2 *shared_data = (s2*) mmap(NULL, data_size, PROT_READ | PROT_WRITE,MAP_SHARED, fd2, 0);

	long long startTime = current_timestamp();

	pid_t pid;
	pid = fork();

	if (pid == -1) {
		perror("There was an error creating the child process");
	}

	if (pid > 0) {			// escritor

		//inicializar o contador de tempo

		//Em cada posiçao do array de estruturas escreve um numero aleatorio
		int j;
		for (j = 0; j < ARRAY_SIZE; j++) {
			shared_data->i = rand() % 200 + 1;
			shared_data->texto[20] = "ISEP – SCOMP 2020";
			shared_data++;
		}

		wait(NULL);
	} else {
		s2 svecleitura[ARRAY_SIZE];
		s2 *ptr = svecleitura;
		while (shared_data->i != 0) {

			int k;
			for (k = 0; k < ARRAY_SIZE; k++) {
				ptr->i = shared_data->i;
				ptr->texto[20] = shared_data->texto;
				ptr++;
				shared_data++;
			}

		}
	}
	long long endTime = current_timestamp();

	printf("O processo demorou %lld ms", endTime - startTime);

	// Undo mapping
	if (munmap((void *) shared_data, ARRAY_SIZE) < 0) {
		printf("Error at munmap()!\n");
		exit(EXIT_FAILURE);
	}

	// Close file descriptor
	if (close(fd) < 0) {
		printf("Error at close()!\n");
		exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}

