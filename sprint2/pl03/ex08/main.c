/*
 * main.c
 *
 *  Created on: 19/04/2020
 *      Author: isep
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For constants “mode” */
#include <fcntl.h> /* For constants O_* */

const int LEITURA = 0;
const int ESCRITA = 1;
const int REPEAT = 1000000;

int main(){
	char* FILE = "/ex08_shm";
	int DATA_SIZE = sizeof(int);


	// 1. Creates and opens a shared memory area
	int fd;
	fd = shm_open(FILE, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);

	// 2. Defines the size
	ftruncate(fd, DATA_SIZE);

	// 3. Get a pointer to the data
	int *sharedData = (int*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE,
	MAP_SHARED, fd, 0);

	//4. Initialize the int

	*sharedData = 100;


	// Fork
	pid_t pid = fork();
	int i;


	// Father and Child Ops
	for (i = 0; i < REPEAT; i++) {
		if (pid > 0) {
			*sharedData = *sharedData + 1;
			*sharedData = *sharedData -1;


		} else if (pid == 0) {
			*sharedData = *sharedData - 1;
			*sharedData = *sharedData + 1;

		}
	}
	if (pid > 0) {
			wait(NULL);
		} else	// If we are at the child
		if (pid == 0) {
			exit(EXIT_SUCCESS);
		}


	printf("============================\n");
	printf("FINAL Value\n");
	printf("FINAL  Value: %d\n", *sharedData);

	printf("============================\n");


		// Undo mapping
	if (munmap((void *) sharedData, DATA_SIZE) < 0) {
		printf("Error at munmap()!\n");
		exit(EXIT_FAILURE);
	}

	// Close file descriptor
	if (close(fd) < 0) {
		printf("Error at close()!\n");
		exit(EXIT_FAILURE);
	}

	// Remove file from system
	if (shm_unlink(FILE) < 0) {
		printf("Error at shm_unlink()!\n");
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}


