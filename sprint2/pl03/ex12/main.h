#include <fcntl.h> /* For constants O_* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For constants “mode” */
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define  DONE  -1
#define  EMPTY     0
#define  FILLED      1

#define STR_SIZE 50
#define NR_DISC 10

typedef struct{
	int numero;
	char nome[STR_SIZE];
	int disciplinas[NR_DISC];
	int status;
}Aluno;

#define SHARED_FILE "/EX12_FILE"
#define DATA_SIZE sizeof(Aluno)

#define SHM_OPEN_OFLAG O_CREAT | O_RDWR | O_TRUNC
#define SHM_OPEN_MODE S_IRUSR | S_IWUSR

