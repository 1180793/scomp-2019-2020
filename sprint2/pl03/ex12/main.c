#include <fcntl.h> /* For constants O_* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For constants “mode” */
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include "main.h"

int main(void){
	int i;
	int fd = shm_open(SHARED_FILE, SHM_OPEN_OFLAG, SHM_OPEN_MODE);

	if (fd == -1) {
		printf("Error at shm_open()\n");
		exit(1);
	}
	ftruncate(fd, DATA_SIZE);
	
	pid_t pid = fork();
	if(pid == -1){
		perror("fork falhou");
		exit(1);
	}
	
	if(pid==0){
		Aluno *sharedData = (Aluno*)mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
		while(sharedData->status != FILLED){}
		
		int maxGrade=0, lowGrade=20, temp;
		double avgGrade=0;
		
		for(i = 0; i<NR_DISC; i++){
			temp = sharedData->disciplinas[i];
			if(temp < lowGrade) lowGrade = temp;
			if(temp > maxGrade) maxGrade = temp;
			avgGrade += temp;
		}
		avgGrade = avgGrade/10;
		printf("Average: %f Max: %d Lowest: %d\n", avgGrade, maxGrade, lowGrade);		
		
	}else{
		Aluno *sharedData = (Aluno*)mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
			
		printf("Introduza o nr, nome e notas do aluno\n");
		
		printf("Nome do aluno:\n");
		char temp[STR_SIZE];
		fgets(temp , STR_SIZE, stdin);
		strcpy(sharedData->nome ,temp);
		
		printf("Número do aluno:\n");
		scanf("%d", &sharedData->numero);
		
		
		
		for(i = 0; i < NR_DISC; i++){
			printf("Nota do aluno na disciplina %d:\n", i+1);
			scanf("%d", &sharedData->disciplinas[i]);
		}
		sharedData->status=FILLED;
	
		wait(NULL);
		
		if (shm_unlink(SHARED_FILE) < 0) {
			printf("Error at shm_unlink()!\n");
			exit(1);
		}
		if (munmap(sharedData, DATA_SIZE) < 0) {
			printf("Error at munmap() no processo filho!\n");
			exit(1);
		}
		if (close(fd) < 0) {
			printf("Error at close() no processo filho!\n");
			exit(1);
		}
	}
	return 0;
}
