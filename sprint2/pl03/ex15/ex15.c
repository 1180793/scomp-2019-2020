#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fcntl.h>

#define SHM_NAME "/shm_ex15"
#define INTS 10

static const int LEITURA = 0;
static const int ESCRITA = 1;

int array[INTS];

static const long int NVALUES = 30;

int main(void) {

	int i = 0, fdProdCons[2], fdConsProd[2];

	if (pipe(fdProdCons) == -1) {
		perror("Pipe failed!");
		exit(-1);
	}
	if (pipe(fdConsProd) == -1) {
		perror("Pipe failed!");
		exit(-1);
	}

	int nEscritos = 0;
	int nLidos = 0;

	pid_t pid;
	pid = fork(); //retorna o valor de pid do processo filho(em caso de sucesso)
				  //ou 0 em caso de ser o processo filho
				  //ou -1 em caso de erro

	if (pid == -1) { //em caso de erro
		perror("Fork failed!");
		exit(EXIT_FAILURE);

	} else if (pid > 0) { 	// Pai: PRODUCER
		//fecha o que não é necessário
		close(fdConsProd[ESCRITA]);
		close(fdProdCons[LEITURA]);

		for (i = 0; i < NVALUES; i++) {

			while ((nEscritos - nLidos) >= INTS); //espera ativa

			array[nEscritos % 10] = i; //preenche uma posicao do array
			nEscritos++;

			//escreve os valores para o consumidor
			write(fdProdCons[ESCRITA], &nEscritos, sizeof(nEscritos));
			write(fdProdCons[ESCRITA], &nLidos, sizeof(nLidos));
			write(fdProdCons[ESCRITA], &array, sizeof(array));

			//le os valores do consumidor
			read(fdConsProd[LEITURA], &nEscritos, sizeof(nEscritos));
			read(fdConsProd[LEITURA], &nLidos, sizeof(nLidos));
			read(fdConsProd[LEITURA], &array, sizeof(array));

		}

		wait(NULL); //espera pelo filho

		//fecha o que já não é necessário
		close(fdProdCons[ESCRITA]);
		close(fdConsProd[LEITURA]);

	} else if (pid == 0) { // Filho: CONSUMER

		//fecha o que não é necessário
		close(fdProdCons[ESCRITA]);
		close(fdConsProd[LEITURA]);

		for (i = 0; i < NVALUES; i++) {

			//le os valores do produtor
			read(fdProdCons[LEITURA], &nEscritos, sizeof(nEscritos));

			read(fdProdCons[LEITURA], &nLidos, sizeof(nLidos));

			read(fdProdCons[LEITURA], &array, sizeof(array));

			while ((nEscritos - nLidos) >= INTS); //espera ativa

			printf("Child Read Value: array[%d] = %d\n", nLidos, array[nLidos % INTS]); //imprime o valor recebido
			nLidos++; //incrementa o num de valores lidos

			//escreve os valores para o produtor
			write(fdConsProd[ESCRITA], &nEscritos, sizeof(nEscritos));

			write(fdConsProd[ESCRITA], &nLidos, sizeof(nLidos));

			write(fdConsProd[ESCRITA], &array, sizeof(array));
		}

		//fecha o que já não é necessário
		close(fdConsProd[ESCRITA]);
		close(fdProdCons[LEITURA]);

		exit(0); //termina o processo
	}

	return 0;
}
