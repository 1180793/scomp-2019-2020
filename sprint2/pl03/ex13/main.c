/*
 * main.c
 *
 *  Created on: 18/04/2020
 *      Author: isep
 */

#include "includes.h"

int main(){

	char words_to_search[NUM_OF_CHILDREN][NAME_SIZE]={"sopa","banana","mel","maçã","fruta","salada","quioske","cantor","claras","portugal"};

    FileData *shared_data;

	int i;

	pid_t pid[NUM_OF_CHILDREN]; //vetor de pids

	int fd1;

	int trunc = 0;

	//Criar memoria partilhada
	fd1 = shm_open(MY_SHARED_FILE, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);

	if(fd1 == -1) {
		perror("Shm_open Error!!");
		exit(-1);
		}

	//Define o tamanho da memoria partilhada
	trunc = ftruncate (fd1, MY_DATA_SIZE);

	if(trunc == -1){
		perror("Ftruncate Error!!");
		exit(-1);
		}

	//Adquirir pointer para a memoria partilhada
	shared_data = (FileData*) mmap(NULL, MY_DATA_SIZE ,PROT_READ|PROT_WRITE, MAP_SHARED, fd1, 0);

	if(shared_data == MAP_FAILED){
		perror("Mmap Error!!");
		exit(-1);
		}


	//Add paths and words to search in struct located in shared memory
	int j=0;
	for(j=0;j<NUM_OF_CHILDREN;j++){
		char pos = (char) j;
		char string[PATH_SIZE] = strcat(FILE_PREFIX,pos);
		string = strcat(string,FILE_SUFIX);
		shared_data->path[j]=string;
		shared_data->word_search[i]=words_to_search[i];
	}

		//Criacao dos filhos
		for(i=0; i<NUM_OF_CHILDREN;i++){

			pid[i] = fork(); //retorna o valor de pid do processo filho(em caso de sucesso)
							//ou 0 em caso de ser o processo filho
							//ou -1 em caso de erro

			if(pid[i] == -1){ //em caso de erro
				printf("Erro na criacaoo do processo filho!!");
				exit(1);

			}else if(pid[i] == 0){ //em caso de ser o processo filho

				filho(i);

			}

		}

		int status[NUM_OF_CHILDREN];

		for(i=0; i<NUM_OF_CHILDREN; i++){
			waitpid(pid[i], &status[i], 0); //coloca o processo pai a espera do processo filho
		}

		for(i=0; i<NUM_OF_CHILDREN; i++){
			printf("shared_data[%d]= %d\n", i, shared_data->occurrences[i]);

		}

		int error = 0;

		error = munmap(shared_data,MY_DATA_SIZE);

		//Tratamento de erros
		if(error == -1) {
			perror("Munmap Error!!");
			exit(-1);
		}

		error = shm_unlink(MY_SHARED_FILE);

		if (error == -1) {
			perror("Erro no shm_unlink");
			exit(1);
		}

		return 0;

	}

	void filho(int i) { //funcao chamada pelo processo filho

		int fd1;

		int trunc = 0;

		FileData *shared_data;

		int data_size = sizeof(FileData);

		//Abrir memoria partilhada
		fd1 = shm_open(MY_SHARED_FILE, O_RDWR, S_IRUSR|S_IWUSR);

		if(fd1 == -1) {
			perror("Shm_open Error!!");
			exit(-1);
		}

		//Define o tamanho da memoria partilhada
		trunc = ftruncate (fd1, data_size);

		if(trunc == -1){
			perror("Ftruncate Error!!");
			exit(-1);
		}

		//Adquirir pointer para memoria partilhada
		shared_data = (FileData*) mmap(NULL, data_size ,PROT_READ|PROT_WRITE, MAP_SHARED, fd1, 0);

		if(shared_data == MAP_FAILED){
			perror("Mmap Error!!");
			exit(-1);
		}

		//Declarar ficheiro
		FILE *fp;

		int line_num = 1;
		int find_result = 0;

		char temp[512];

		if((fp = fopen(shared_data->path[i], "r")) == NULL){
			printf("File Error!!");
			exit(EXIT_FAILURE);
		}

		//Procurar palavra a desejada para o processo i copiando os conteudos do seu ficheiro input para uma string temporaria
		while(fgets(temp, 512, fp) != NULL) {

			if((strstr(temp, shared_data->word_search[i])) != NULL) {
				//printf("A match found on line: %d\n", line_num);
				find_result++;
			}

			line_num++;
		}

		printf("find_result (son number %d) -> %d\n", i, find_result);

		//Escrever ocorrencias na area partilhada de memoria
		shared_data->occurrences[i]=find_result;

		int error = 0;

		error = munmap(shared_data,data_size);

		//Tratamento de erros
		if(error == -1) {
			perror("Munmap Error!!");
			exit(-1);
		}

		exit(1); //termina o processo filho
	}
