/*
 * includes.h
 *
*  Created on: 27/03/2020
 *      Author: isep
 */

#ifndef INCLUDES_H_
#define INCLUDES_H_

#include <fcntl.h> /* For constants O_* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For constants “mode” */
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>


#define FILE_PREFIX "child_"
#define FILE_SUFIX ".txt"
#define MY_SHARED_FILE "/EX13_SHM_FILE"
#define NUM_OF_CHILDREN 10
#define NAME_SIZE 20
#define PATH_SIZE 20

typedef struct{
	char path[NUM_OF_CHILDREN][PATH_SIZE];
	char word_search[10];
	int occurrences[10];
}FileData;

#define MY_DATA_SIZE sizeof(FileData)

#define MY_SHM_OPEN_WRITER_OFLAG O_CREAT | O_EXCL | O_RDWR
#define MY_SHM_OPEN_WRITER_MODE S_IRUSR | S_IWUSR

#define MY_SHM_OPEN_READER_OFLAG O_EXCL | O_RDWR
#define MY_SHM_OPEN_READER_MODE S_IRUSR | S_IWUSR

void filho(int i);

#endif /* INCLUDES_H_ */
