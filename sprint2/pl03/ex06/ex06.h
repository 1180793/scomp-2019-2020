#include <fcntl.h> /* For constants O_* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For constants “mode” */
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define DONE  -1
#define EMPTY     0
#define FILLED      1

typedef struct{
	int numero;
	int status;
}passagem;

#define SHARED_FILE "/EX06_FILE"
#define DATA_SIZE sizeof(passagem)

#define SHM_OPEN_WRITER_OFLAG O_CREAT | O_RDWR
#define SHM_OPEN_WRITER_MODE S_IRUSR | S_IWUSR

#define SHM_OPEN_READER_OFLAG O_EXCL | O_RDWR
#define SHM_OPEN_READER_MODE S_IRUSR | S_IWUSR

#define LEITURA		0
#define ESCRITA 	1
#define ARRAY_SIZE  10
#define BUFFER_SIZE sizeof(passagem)
