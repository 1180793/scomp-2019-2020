#include "ex06.h"

int main(void){
	int vec1[ARRAY_SIZE];
    int vec2[ARRAY_SIZE];
    int i;
    time_t t; //for srand
    /* intializes RNG (srand():stdlib.h; time(): time.h) */
    srand((unsigned)time(&t));

    /* initialize array with random numbers (rand(): stdlib.h) */
    
    for (i = 0; i < ARRAY_SIZE; i++)
    {
        vec1[i] = rand() % (10);
        printf("%d\n", i);
    }
    printf("fim preencher array\n");
    
//----------------------------------------------------------PIPES
	
	int fd[2];
	
	if (pipe(fd) == -1)
    {
        perror("Pipe failed");
        return 1;
    }
    clock_t start = clock(); //inicio contagem
	
	pid_t pid = fork();
	
	if(pid == -1){
		perror("fork falhou");
		exit(1);
	}
	
	if(pid > 0){
		close(fd[LEITURA]);
		for(i=0 ; i<ARRAY_SIZE ; i++){
			write(fd[ESCRITA] , &vec1[i] , DATA_SIZE);
		}
		close(fd[ESCRITA]);
		printf("fim escrita pipe\n");
		}
	else{
		close(fd[ESCRITA]);
		for(i=0 ; i<ARRAY_SIZE ; i++){
			read(fd[LEITURA] , &vec2[i] , DATA_SIZE);
		}
		close(fd[LEITURA]);
		printf("fim leitura pipe\n");
	}
	clock_t end = clock(); // fim contagem
	float pipeTime = (float)(end - start) / CLOCKS_PER_SEC;
//-------------------------------------------------------FIM  PIPES
	
//--------------------------------------------------------Inicio SHM
	int fd2 = shm_open(SHARED_FILE, SHM_OPEN_WRITER_OFLAG, SHM_OPEN_WRITER_MODE);

	if (fd2 == -1) {
		printf("Error at shm_open() no processo filho!\n");
		exit(1);
	}
		 
	ftruncate(fd2, DATA_SIZE);

	passagem *sharedData = (passagem*)mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd2, 0);
	sharedData->status=EMPTY;
	
	start = clock(); //inicio contagem
	pid_t pid2 = fork();
	
	if(pid2 == -1){
		perror("fork falhou\n");
		exit(1);
	}
	
	if(pid2 > 0){
		for(i=0 ; i<ARRAY_SIZE ; i++){
			while(sharedData->status==FILLED){}
			sharedData->numero=vec1[i];
			sharedData->status=FILLED;
		}
		printf("fim escrita shm\n");
	}
	else{
		for(i=0 ; i<ARRAY_SIZE ; i++){
			while(sharedData->status==EMPTY){}
			vec2[i]=sharedData->numero;
			sharedData->status=EMPTY;
		}
		printf("fim leitura shm\n");
	}
	end = clock(); // fim contagem
	if (munmap(sharedData, DATA_SIZE) < 0) 
	{ 
		printf("Error at munmap() no processo filho!\n"); 
		exit(1); 
	}  
	if (close(fd2) < 0) 
	{ 
		printf("Error at close() no processo filho!\n"); 
		exit(1);
	} 
	if (shm_unlink(SHARED_FILE) < 0) 
	{ 
		printf("Error at shm_unlink()!\n"); 
		exit(1);
	}
	
	float SHM_Time = (float)(end - start) / CLOCKS_PER_SEC;
	printf("Pipes: %4.3f  SHM: %4.3f \n", &pipeTime, &SHM_Time);
	
	return 0;
}
    
