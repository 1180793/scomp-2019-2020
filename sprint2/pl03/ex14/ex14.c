#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fcntl.h>

#define SHM_NAME "/shm_ex14"
#define INTS 10

typedef struct {
	int array[INTS];
	int nEscritos;
	int NLidos;
} struct14;

static const long int NVALUES = 30;

int main(void) {

	int i = 0, fd, trunc = 0;

	int data_size = sizeof(struct14);
	struct14* shared_data;

	pid_t pid;

	fd = shm_open(SHM_NAME, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
	if (fd == -1) {
		perror("Shm_open failed!");
		exit(EXIT_FAILURE);
	}

	//Define o tamanho da memória partilhada
	trunc = ftruncate(fd, data_size);
	if (trunc == -1) {
		perror("Ftruncate failed!");
		exit(EXIT_FAILURE);
	}

	//Mapear a memoria partilhada
	shared_data = mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (shared_data == MAP_FAILED) {
		perror("Mmap failed!");
		exit(EXIT_FAILURE);
	}

	pid = fork(); //retorna o valor de pid do processo filho(em caso de sucesso)
				  //ou 0 em caso de ser o processo filho
				  //ou -1 em caso de erro

	if (pid == -1) { //em caso de erro
		perror("Fork failed!");
		exit(EXIT_FAILURE);

	} else if (pid > 0) { 	// Pai: PRODUCER
		for (i = 0; i < NVALUES; i++) {
			while ((shared_data->nEscritos - shared_data->NLidos) >= INTS); //espera ativa
			shared_data->array[shared_data->nEscritos % INTS] = i;
			shared_data->nEscritos++;
		}

		wait(NULL); //espera pelo filho

		int error = 0;
		error = munmap(shared_data, data_size);

		//Tratamento de erros
		if (error == -1) {
			perror("Munmap failed!");
			exit(EXIT_FAILURE);
		}

		error = shm_unlink(SHM_NAME);
		if (error == -1) {
			perror("Shm_unlink failed!");
			exit(EXIT_FAILURE);
		}

	} else if (pid == 0) { // Filho: CONSUMER!
		for (i = 0; i < NVALUES; i++) {
			while (shared_data->nEscritos <= shared_data->NLidos); //espera ativa
			printf("Value Read by Child: %d\n", shared_data->array[shared_data->NLidos % INTS]);
			shared_data->NLidos++;
		}

		int error = 0;
		error = munmap(shared_data, data_size);

		//Tratamento de erros
		if (error == -1) {
			perror("Munmap failed!");
			exit(EXIT_FAILURE);
		}
		exit(0);
	}

	return 0;
}
