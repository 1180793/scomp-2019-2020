#include "main.h"

int main(int argc, char *argv[]) {
	int fd = shm_open(SHARED_FILE, SHM_OPEN_READER_OFLAG, SHM_OPEN_READER_MODE);

    if (fd == -1) {
        printf("Error at shm_open()!\n");
        exit(EXIT_FAILURE);
    }
     
    ftruncate(fd, DATA_SIZE);

    Student *sharedData = (Student*)mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    
    printf("student number: %d \n", &sharedData->number);
    printf("student name: %s \n", &sharedData->name);

    if (munmap(sharedData, DATA_SIZE) < 0) {
        printf("Error at munmap()!\n");
        exit(1);
    }
    
    if (close(fd) < 0) {
        printf("Error at close()!\n");
        exit(1);
    }

    if (shm_unlink(SHARED_FILE) < 0) {
        printf("Error at shm_unlink()!\n");
        exit(1);
    }

    return 0;
}
