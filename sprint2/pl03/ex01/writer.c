#include "main.h"

int main(int argc, char *argv[]) {
	
	int fd = shm_open(SHARED_FILE, SHM_OPEN_WRITER_OFLAG, SHM_OPEN_WRITER_MODE);

    if (fd == -1) {
        printf("Error at shm_open()!\n");
        exit(EXIT_FAILURE);
    }
     
    ftruncate(fd, DATA_SIZE);

    Student *sharedData = (Student*)mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	printf("Nome do aluno:\n");
	//getchar();
	char str[80];
	//fgets(str,80,stdin);
	
	////Ultima posição
	//str[strlen(str)-1]='\0';
	scanf("%s", &str);
	strcpy(sharedData->name,str);
	
	printf("Número do aluno:\n");
	scanf("%d", &sharedData->number);
	
	 if (munmap(sharedData, DATA_SIZE) < 0) {
        printf("Error at munmap()!\n");
        exit(1);
    }

    if (close(fd) < 0) {
        printf("Error at close()!\n");
        exit(1);
    }
    
    return 0;

}
