/*
 * ex11.c
 *
 *  Created on: 22/04/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#define SIZE 1000
#define N_CHILDS 10

int main(){

	int i;
	int randArray[SIZE];
	int fd[2];
	pid_t pid;
	int max = 0;

	for( i = 0; i < SIZE; i++){

		randArray[i] = random() % 1000;
	}

	if( pipe(fd) == -1){
			perror("Pipe failed");
			return -1;
	}

	for( i = 0; i < N_CHILDS; i++){

			pid = fork();

			if(pid == 0){

				int startIndex = i * 100;


				for( i = startIndex; i < startIndex + 100; i++){

					if( randArray[i] > max )
						max = randArray[i];
				}

				close(fd[0]);
				write(fd[1], &max, sizeof(int));
				close(fd[1]);

				return 0;
			}
	}

	//Waits for the Child processes to finish
	for(i = 0; i < N_CHILDS; i++){
			wait(NULL);
	}

	//Determines the Max value
	int maxArray[N_CHILDS];

	close(fd[1]);
	for(i = 0; i < N_CHILDS; i ++){

		read(fd[0], &maxArray[i], sizeof(int));
		if(maxArray[i] > max){
			max = maxArray[i];
		}
	}
	close(fd[0]);

	printf("Max Value in the Random Array: %d\n", max);

	return 0;

}
