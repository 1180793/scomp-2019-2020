/*
 * writer.c
 *
 *  Created on: 18/04/2020
 *      Author: isep
 */


#include "includes.h"


int main(){

	//1. create and open shared memory region
	int fd = shm_open(MY_SHARED_FILE, MY_SHM_OPEN_WRITER_OFLAG, MY_SHM_OPEN_WRITER_MODE);

	if (fd == -1){
		printf("Error at shm_open()!\n");
		        exit(EXIT_FAILURE);
	}

	//2. define region size
	ftruncate(fd,MY_DATA_SIZE);

	//3. get a pointer to the data
    int *sharedData =(int *) mmap(NULL,MY_DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    //4. fill array and place in shared memory
    int array[11];

    int i = 0;

    for (i=0; i<10; i++){
    	array[i]=rand()%20+1;
    }
    array[STATUS_INT_POS]=1;

    sharedData = array;

    //5. unmap
    if (munmap((int *)sharedData,MY_DATA_SIZE) < 0) {
        printf("Error at munmap()!\n");
        exit(EXIT_FAILURE);
    }

    //6. close file descriptor
    if (close(fd) < 0) {
        printf("Error at close()!\n");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;

}

