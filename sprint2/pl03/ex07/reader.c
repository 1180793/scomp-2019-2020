/*
 * reader.c
 *
 *  Created on: 18/04/2020
 *      Author: isep
 */


#include "includes.h"



int main(){

	//1. create and open shared memory region
		int fd = shm_open(MY_SHARED_FILE, MY_SHM_OPEN_WRITER_OFLAG, MY_SHM_OPEN_WRITER_MODE);

		if (fd == -1){
			printf("Error at shm_open()!\n");
			        exit(EXIT_FAILURE);
		}

	//2. define region size
		ftruncate(fd, MY_DATA_SIZE);

    //3. Get a pointer to the data
	    int *sharedData = (int *)mmap(NULL, MY_DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	//4.Wait for writer
	    while(*(sharedData+sizeof(int)*STATUS_INT_POS)==0){}

	//5. Calculate average of numbers.
	    int avg = 0;

	    int j=0;

	    for (j=0;j<10;j++){
	    	avg=avg+*sharedData;
	    	sharedData++;
	    }

	    avg = avg/10;
	    printf("The average of the numbers is:%d",avg);
	//6. unmap
	        if (munmap((int *)sharedData,MY_DATA_SIZE) < 0) {
	            printf("Error at munmap()!\n");
	            exit(EXIT_FAILURE);
	        }

	//7. close file descriptor
	        if (close(fd) < 0) {
	            printf("Error at close()!\n");
	            exit(EXIT_FAILURE);
	        }

	    return avg;
}
