/*
 * includes.h
 *
*  Created on: 27/03/2020
 *      Author: isep
 */

#ifndef INCLUDES_H_
#define INCLUDES_H_

#include <fcntl.h> /* For constants O_* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For constants “mode” */
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define MY_SHARED_FILE "/EX07_SHM_FILE"
#define MY_DATA_SIZE sizeof(int)*11
#define STATUS_INT_POS 11

#define MY_SHM_OPEN_WRITER_OFLAG O_CREAT | O_EXCL | O_RDWR
#define MY_SHM_OPEN_WRITER_MODE S_IRUSR | S_IWUSR

#define MY_SHM_OPEN_READER_OFLAG O_EXCL | O_RDWR
#define MY_SHM_OPEN_READER_MODE S_IRUSR | S_IWUSR


#endif /* INCLUDES_H_ */
