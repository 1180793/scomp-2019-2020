/*
 * ex09.c
 *
 *  Created on: 08/05/2020
 *      Author: isep
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#define SIZE 1000
#define N_CHILDS 10
#define SHM_NAME "/shm_ex09"

int main(){

	int i;
	int randArray[SIZE];
	int fd;
	int r;
	int ft;
	int data_size = N_CHILDS * sizeof(int);
	int* addr;
	pid_t pid[N_CHILDS];
	int status[N_CHILDS];
	int max = 0;

	srand((unsigned) time(NULL));
	for( i = 0; i < SIZE; i++){

		randArray[i] = random() % 1000;
	}


	fd = shm_open(SHM_NAME,  O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	if(fd == -1) {
			perror("shm_open error...");
			exit(EXIT_FAILURE);
	}


	ft = ftruncate (fd, data_size);
	if(ft == -1){
		perror("ftruncate error...");
		exit(EXIT_FAILURE);
	}

	addr = (int*) mmap(NULL, data_size,PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	//Initializes the shared memory area
	for(i = 0; i < N_CHILDS ; i++){
		addr[i] = -1;
	}

	for( i = 0; i < N_CHILDS; i++){

			pid[i] = fork();

			if(pid[i] == 0){

				int j;

				int startIndex = i * 100;

				for( j = startIndex; j < startIndex + 100; j++){

					if( randArray[j] > max )
						max = randArray[j];
				}

				addr[i] = max;

				return 1;
			}
	}

	//waits for the Child processes to finish
	for(i=0; i<N_CHILDS; i++){
		waitpid(pid[i], &status[i], 0);
	}

	//Determines the Max value
	for(i = 0; i < N_CHILDS; i++){
		if(addr[i] > max){
			max = addr[i];
		}
		printf("Max value section %d: %d\n",i+1, addr[i]);
	}

	printf("Max value in the Random Array: %d\n", max);

	r = munmap(addr, data_size);
	if(r == -1){
		perror("munmap error...");
		exit(EXIT_FAILURE);
	}

	r = close(fd);
	if(r == -1){
		perror("close error...");
		exit(EXIT_FAILURE);
	}

	r = shm_unlink(SHM_NAME);
	if(r == -1){
		perror("shm_unlink error...");
		exit(EXIT_FAILURE);
	}

	return 0;

}
