/*
 * reader.c
 *
 *  Created on: 27/03/2020
 *      Author: isep
 */

#include "includes.h"



int main(){

	//1. create and open shared memory region
		int fd = shm_open(MY_SHARED_FILE, MY_SHM_OPEN_WRITER_OFLAG, MY_SHM_OPEN_WRITER_MODE);

		if (fd == -1){
			printf("Error at shm_open()!\n");
			        exit(EXIT_FAILURE);
		}

	//2. define region size
		ftruncate(fd, MY_DATA_SIZE);

    //3. Get a pointer to the data
	    Product *sharedData = (Product*)mmap(NULL, MY_DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	//4. Print data
	    printf("Product code:%d\n",&sharedData->code);
	    printf("Product description:%s\n",&sharedData->description);
	    printf("Product price:%f\n",&sharedData->price);

	//5.Esperar por sinal
	    while(sharedData->canRead!=1){}

	//6. unmap
	       if (munmap((void *)sharedData,MY_DATA_SIZE) < 0) {
	           printf("Error at munmap()!\n");
	           exit(EXIT_FAILURE);
	       }

    //7. close file descriptor
	       if (close(fd) < 0) {
	           printf("Error at close()!\n");
	           exit(EXIT_FAILURE);
	       }
return EXIT_SUCCESS;
}
