/*
 * ex02.c
 *
 *  Created on: 15 May 2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "header.h"

void* print_fields(void *arg){

	student stud = *((student*) arg);

	printf("%s, student number %d, with a grade of %c.\n", stud.name, stud.number, stud.grade);

	int *ret = (int*) malloc(sizeof(int));

	*ret = stud.number - 1;

	pthread_exit((void*) ret);
}

int main(){

	int i;
	student stud[ARRAY_SIZE];
	pthread_t threads[ARRAY_SIZE];
	char names[ARRAY_SIZE][NAME_SIZE] = { "Jeff", "Abed", "Chang", "Britta", "Pierce"};
	char grades[ARRAY_SIZE] = {'B', 'A', 'F', 'C', 'C'};
	void *ret;
	int tNum;

	//Fill the array
	for( i = 0; i < ARRAY_SIZE; i++){

		stud[i].number = i + 1;
		strcpy(stud[i].name, names[i]);
		stud[i].grade = grades[i];
	}

	//Thread creation
	for( i = 0; i < ARRAY_SIZE; i++)
		pthread_create(&threads[i], NULL, print_fields, (void*) &stud[i]);

	//Retrieves the thread return values and waits for them to finish
	for( i = 0; i < ARRAY_SIZE; i++){
		pthread_join(threads[i], (void*) &ret);
		tNum = *( (int*) ret);
		printf("Thread number %d has terminated.\n", tNum);
	}

	free(ret);

	return 0;

}
