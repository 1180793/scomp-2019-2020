/*
 * header.h
 *
 *  Created on: 20 May 2020
 *      Author: isep
 */

#ifndef HEADER_H_
#define HEADER_H_

void* print_fields(void *arg);

#define NAME_SIZE 100
#define ARRAY_SIZE 5

typedef struct{
	int number;
	char name [NAME_SIZE];
	char grade;
}student;


#endif /* HEADER_H_ */
