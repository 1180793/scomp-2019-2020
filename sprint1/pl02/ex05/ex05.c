/*
 * ex05.c
 *
 *  Created on: 20/03/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

#define MAX_SIZE 256

int main(int n){

	int fdU[2];
	int fdD[2];
	char msg[MAX_SIZE];
	char msgCase[MAX_SIZE];
	int i = 0;
	pid_t pid;

	if( pipe(fdU) == -1){
		perror("PipeUP failed");
		return -1;
	}
	if( pipe(fdD) == -1){
		perror("PipeDown failed");
		return -1;
	}

	pid = fork();

	if( pid > 0){

		close(fdU[1]);
		read(fdU[0], &msgCase, MAX_SIZE);
		close(fdU[0]);

		while( msgCase[i] != '\0'){
			if(msgCase[i] >= 'a' && msgCase[i] <= 'z'){
				msgCase[i] -= 32;
			}else if( msgCase[i] >= 'A' && msgCase[i] <= 'Z'){
				msgCase[i] += 32;
			}
			i++;
		}

		close(fdD[0]);
		write(fdD[1], &msgCase, MAX_SIZE);
		close(fdD[1]);

	}else if(pid == 0){

		printf("Type a message...\n");
		fgets(msg, MAX_SIZE, stdin);

		close(fdU[0]);
		write(fdU[1], &msg, MAX_SIZE);
		close(fdU[1]);

		close(fdD[1]);
		read(fdD[0], &msg, MAX_SIZE);
		close(fdD[0]);

		printf("Message converted: %s\n", msg);
	}else{
		perror("Fork Failed");
		return -1;
	}

	return 0;
}
