/*
 * ex11.c
 *
 *  Created on: 22/03/2020
 *      Author: isep
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <sys/wait.h>
#include <time.h>
#include "ex11.h"

#define ARRAY_SIZE 1000
#define NUMBER_RANGE 500
#define RANGE 200
#define CHILDS 5

static const int LEITURA = 0;
static const int ESCRITA = 1;

	/**
	 * Funçao a ser chamada pelo processo pai
	 */
	void pai(int fd[6][2],int num, pid_t* pid){
		close(fd[0][LEITURA]);// fecha o pipe da leitura q nao vai ser necessario

		srand(time(NULL));
		num = rand() % NUMBER_RANGE +1; // numeros entre 1-500

		printf("Nº do pai = %d\n",num);
		// escrever o numero gerado para o pipe
		write(fd[0][ESCRITA],&num,sizeof(int));

		int status;
		int i;
		int aux=0;

		for(i=0;i<CHILDS;i++){
			waitpid(pid[i],&status,0); //processo pai espera pelo filho
		}

		read(fd[5][LEITURA],&aux,sizeof(int)); // reads the number of the 5th child process

		//imprime o numero maior final
		printf("Maior numero obtido é %d\n",aux);

		//Fecha o pipe

		close(fd[0][ESCRITA]);
		close(fd[5][LEITURA]);
	}

	/**
	 * Funçao a ser chamada pelo processo filho
	 */
	void filho(int fd[6][2],int i, int num1,int num2){
		int j;
		for(j=0;j<i-1;j++){
			if(j!=i){
				close(fd[j][ESCRITA]);
				close(fd[j][LEITURA]);
			}
		}
		close(fd[i][ESCRITA]);

		srand(time(NULL) ^ getpid()<<4);
		num1 = rand() % NUMBER_RANGE +1;

		printf("numero do filho %d --> %d\n",i,num1);

		read(fd[i][LEITURA],&num2,sizeof(int));
		//se o num1 obtido for superior ao num2 lido pelo pipe
		if(num1>num2){
			//envia o valor de num1 para o pipe
			close(fd[i+1][LEITURA]);
			write(fd[i+1][ESCRITA],&num1,sizeof(int));
			close(fd[i+1][ESCRITA]);

		}else{ // se o num1 gerado for inferior ao num2 lido pelo pipe
			close(fd[i+1][LEITURA]);
			write(fd[i+1][ESCRITA],&num2,sizeof(int));
			close(fd[i+1][ESCRITA]);

		}
		close(fd[i][LEITURA]);
		exit(1);
	}

	int main(void){
		int i;
		pid_t pid[CHILDS]; //vetor de pids
		int fd[CHILDS+1][2]; // 1 pipe por processo
		int num1=0;
		int num2=0;

		for(i=0;i<CHILDS + 1;i++){
			if(pipe(fd[i]) == -1){
				perror("Erro na criaçao do pipe!!!");
				exit(-1);
			}
		}
		for(i=0;i<CHILDS;i++){
			pid[i]= fork();

			if(pid[i]==-1){
				printf("Erro na criaçao do processo filho!");
				exit(1);
			}else if(pid[i]==0){
				filho(fd,i,num1,num2);
			}
		}

		pai(fd,num1,pid);
		return 0;
	}
