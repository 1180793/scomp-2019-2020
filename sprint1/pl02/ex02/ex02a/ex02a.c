#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

#define STRING_SIZE 250

static const int LEITURA = 0;
static const int ESCRITA = 1;

int main(void) {

	int fd[2], num;
	char str[STRING_SIZE];

	if (pipe(fd) == -1) {
		perror("Pipe Failed!\n");
		return EXIT_FAILURE;
	}

	pid_t pid;
	pid = fork();

	if (pid == -1) {
		perror("Fork Failed!\n");
		return EXIT_FAILURE;
	} else if (pid > 0) {	// Processo Pai
		printf("Enter Integer: ");
		scanf("%d%*c", &num);

		printf("Enter String: ");
		fgets(str, STRING_SIZE, stdin);

		close(fd[LEITURA]);
		write(fd[ESCRITA], &num, sizeof(int));
		write(fd[ESCRITA], &str, sizeof(char) * (strlen(str) + 1));	// +1 porque termina em '\0')
		close(fd[ESCRITA]);

	} else if (pid == 0) {	// Processo Filho

		close(fd[ESCRITA]);

		int intValue = read(fd[LEITURA], &num, sizeof(int));
		if (intValue > 0) {
			printf("Integer: %d\n", num);
		} else {
			perror("Error reading integer!");
			return EXIT_FAILURE;
		}

		char c;
		int i = 0;
		while (read(fd[LEITURA], &c, sizeof(char)) > 0) {
			str[i] = c;
			if (c == '\0') {	// Fim da String
				printf("String: %s\n", str);
			}
			i++;
		}
		close(fd[LEITURA]);

	}

	return EXIT_SUCCESS;

}
