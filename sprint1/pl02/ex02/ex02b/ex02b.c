#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

#define STRING_SIZE 250

static const int LEITURA = 0;
static const int ESCRITA = 1;

typedef struct {
	int num;
	char str[STRING_SIZE];
} estrutura;

int main(void) {

	int fd[2];
	estrutura est;

	if (pipe(fd) == -1) {
		perror("Pipe Failed!\n");
		return EXIT_FAILURE;
	}

	pid_t pid;
	pid = fork();

	if (pid == -1) {
		perror("Fork Failed!\n");
		return EXIT_FAILURE;
	} else if (pid > 0) {	// Processo Pai

		printf("Enter Integer: ");
		scanf("%d%*c", &est.num);

		printf("Enter String: ");
		fgets(est.str, STRING_SIZE, stdin);

		close(fd[LEITURA]);
		write(fd[ESCRITA], &est, sizeof(est));
		close(fd[ESCRITA]);

	} else if (pid == 0) {	// Processo Filho

		close(fd[ESCRITA]);

		int value = read(fd[LEITURA], &est, sizeof(est));
		if (value > 0) {
			printf("Integer: %d\n", est.num);
			printf("String: %s\n", est.str);
		} else {
			perror("Error reading struct!");
			return EXIT_FAILURE;
		}

		close(fd[LEITURA]);

	}

	return EXIT_SUCCESS;

}
