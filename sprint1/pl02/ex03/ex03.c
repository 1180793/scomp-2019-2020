/*
 * ex03.c
 *
 *  Created on: 21/03/2020
 *      Author: isep
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <string.h>
#define BUFFER_SIZE 80

static const int LEITURA = 0;
static const int ESCRITA = 1;

int main(void) {

	int fd[2];

	if (pipe(fd) == -1) { //Verifica se nao ocorreu erro no pipe
		printf("Pipe Failed!\n");
		return EXIT_FAILURE;
	}

	pid_t pid;
	pid = fork();

	if (pid == -1) { //Verifica se nao ocorreu erro no fork
		printf("Fork Failed!\n");
		return EXIT_FAILURE;

	} else if (pid > 0) {	// Processo Pai

		close(fd[LEITURA]);	// Fecha extremidade nao usada

		char msg[BUFFER_SIZE] = 'Hello World';
		char msg1[BUFFER_SIZE] = 'Goodbye';


		write(fd[ESCRITA], msg, sizeof(msg));
		write(fd[ESCRITA], msg1, sizeof(msg));

		close(fd[ESCRITA]);	// Fecha extremidade de escrita

	} else if (pid == 0) { //Processo filho

		close(fd[ESCRITA]); // Fecha extremidade nao usada

		char read_msg[BUFFER_SIZE];
		char read_msg1[BUFFER_SIZE];


		int rValueMsg = read(fd[LEITURA], read_msg, sizeof(read_msg));
		int rValueMsg1 = read(fd[LEITURA], read_msg1, sizeof(read_msg1));
		close(fd[LEITURA]);  // Fecha extremidade de leitura

		if(rValueMsg == -1 || rValueMsg1 == -1){
			printf("Ocorreu um erro na operaçao de leitura");
			return EXIT_FAILURE;
		}
		printf("readmsg-> %s\n",read_msg);
		printf("readmsg1-> %s\n",read_msg1);


	}

	return EXIT_SUCCESS;

}

