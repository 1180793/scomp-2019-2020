#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

const int INITIAL_CREDITS = 20;
const int NUMBER_RANGE = 5;

const int LEITURA = 0;
const int ESCRITA = 1;

const int WIN = 10;
const int LOSS = -5;

const int HAS_CREDITS = 1;
const int END_GAME = 0;

int main(void) {

	int fd[2][2], i;

	for (i = 0; i < 2; i++) {
		if (pipe(fd[i]) == -1){
			perror("Pipe Failed!\n");
			return EXIT_FAILURE;
		}
	}

	int credits = INITIAL_CREDITS, random, numChild, flag = 1;
	printf("Starting Betting System....\n");
	printf("Current Credits: %d\n", credits);
	printf("-------------------------------------\n");

	pid_t pid = fork();
	if (pid == -1){
		perror("Fork Failed!\n");
		return EXIT_FAILURE;
	}

	if (pid > 0) {	// Father
		srand(time(NULL));

		close(fd[0][LEITURA]);
		close(fd[1][ESCRITA]);

		while(credits > 0) {
			flag = HAS_CREDITS;
			random = rand () % NUMBER_RANGE + 1; // Numbers between 1 and NUMBER_RANGE
			printf("RANDOM NUMBER WAS: %d\n", random);
			write(fd[0][ESCRITA], &flag, sizeof(int)); // Sends 'flag' though pipe telling child it still has credits
			read(fd[1][LEITURA], &numChild, sizeof(int));	// Reads child bet

			if (random == numChild) {
				credits = credits + WIN;
				printf("You win! The child bet was: %d\n", numChild);
			} else {
				credits = credits + LOSS;
				printf("You lose! The child bet was: %d\n", numChild);
			}
			printf("Current Credits: %d\n", credits);
			printf("-------------------------------------\n");
		}
		flag = END_GAME;
		write(fd[0][ESCRITA], &flag, sizeof(int));
		close(fd[0][ESCRITA]);
		close(fd[1][LEITURA]);

	} else if (pid == 0) { // Child
		srand(time(NULL) * getpid()); // Changed seed so it doesn't match the generated value by the father

		close(fd[0][ESCRITA]);
		close(fd[1][LEITURA]);

		int hasCredits = 1, num;

		while(hasCredits) {
			read(fd[0][LEITURA], &hasCredits, sizeof(int));
			num = rand() % NUMBER_RANGE + 1;
			write(fd[1][ESCRITA], &num, sizeof(int));
		}

		close(fd[1][ESCRITA]);
		close(fd[0][LEITURA]);
		exit(EXIT_SUCCESS);
	}

	return EXIT_SUCCESS;
}
