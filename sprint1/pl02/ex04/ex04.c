#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>


#define BUFFER_SIZE 1000

static const int READ = 0;
static const int WRITE = 1;

int main(){
	char str[BUFFER_SIZE];
	  int fd[2], aux , aux2;

	  pipe(fd);
	  
	  pid_t p = fork();

	  if(p>0) {	//pai
		  
	    close(fd[READ]);

	    aux = open("input.txt", O_RDONLY);	// Leitura do ficheiro
	    
	    while((aux2 = read(fd[READ], str, BUFFER_SIZE)) !=0){;
			write(fd[WRITE], str, strlen(str));	//Escrita para o pipe
		}
		
			wait(NULL);
			close(aux);
		
		close(fd[WRITE]);	//Conclui-se e fecha-se
	    
	  } else {	//Processo filho
	    
	    close(fd[WRITE]);

	    read(fd[WRITE], str, sizeof(str));	//Leitura do pipe
	    close(fd[WRITE]);

		//Output
	    int i = 0;
	    printf("Received string is:");
	    while(str[i] != '\0'){
			printf("%c", str[i]);
			i++;
		}
		printf("\n");
	  }
	    
	  return 0;

}
