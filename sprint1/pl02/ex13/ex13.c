/*
 * ex13.c
 *
 *  Created on: 22/03/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include "header.h"

#define SIZE 1000
#define N_MACHINES 4
#define PIECE5 5
#define PIECE10 10
#define PIECE100 100

int main(int n){

	int fdM1M2[2];
	int fdM2M3[2];
	int fdM3M4[2];
	int fdM4SA[2];
	int i;
	int counter = SIZE;
	pid_t pid;
	piece pieceArray[SIZE];
	piece tempPiece5[PIECE5];
	piece tempPiece10[PIECE10];
	piece tempPiece100[PIECE100];

	/* Fill the array with random numbers
	 */
	for( i = 0; i < SIZE; i++){
		piece p;

		p.pieceID = i + 1;
		p.height = random() % 100;
		p.length = random() % 100;
		p.width = random() % 100;

		pieceArray[i] = p;
	}

	//Creation of the pipes to represent the flux of pieces
	if( pipe(fdM1M2) == -1){
		perror("Pipe failed");
		return -1;
	}
	if( pipe(fdM2M3) == -1){
		perror("Pipe failed");
		return -1;
	}
	if( pipe(fdM3M4) == -1){
		perror("Pipe failed");
		return -1;
	}
	if( pipe(fdM4SA) == -1){
		perror("Pipe failed");
		return -1;
	}

	for( i = 0; i < N_MACHINES; i++){

		pid = fork();


		if(pid == 0){

			switch(i){
				case 0:
					//Machine 1 cuts 5 pieces from pieceArray and sends them to Machine 2
					close(fdM1M2[0]);
					while(counter > 0){
						printf("Machine 1 cuts 5 pieces\n");
						int startIndex = SIZE - counter;
						for(i = startIndex; i < (startIndex + PIECE5); i++)
							write(fdM1M2[1], &pieceArray[i], sizeof(piece));

						counter -= PIECE5;
					}
					close(fdM1M2[1]);
					printf("Machine 1 shutting down...\n");

					return 0;

				case 1:
					//Machine 2 reads 5 pieces from Machine 1, folds them and sends to Machine 3
					close(fdM1M2[1]);
					close(fdM2M3[0]);

					while(counter > 0){
						for(i = 0; i < PIECE5; i++)
							read(fdM1M2[0], &tempPiece5[i], sizeof(piece));

						printf("Machine 2 folds 5 pieces\n");
						counter -= PIECE5;

						for(i = 0; i < PIECE5; i++)
							write(fdM2M3[1], &tempPiece5[i], sizeof(piece));
					}

					close(fdM1M2[0]);
					close(fdM2M3[1]);
					printf("Machine 2 shutting down...\n");

					return 0;

				case 2:
					//Machine 3 waits for 10 pieces from Machine 2, welds the pieces and sends them to Machine 4
					close(fdM2M3[1]);
					close(fdM3M4[0]);

					while(counter > 0){
						for(i = 0; i < PIECE10; i++)
							read(fdM2M3[0], &tempPiece10[i], sizeof(piece));

						printf("Machine 3 welds 10 pieces\n");
						counter -= PIECE10;

						for(i = 0; i < PIECE10; i++)
							write(fdM3M4[1], &tempPiece10[i], sizeof(piece));
						}

					close(fdM2M3[0]);
					close(fdM3M4[1]);
					printf("Machine 3 shutting down...\n");

					return 0;

				case 3:
					//Machine 4 waits for 100 pieces to be ready, packs and sends them to the Storage Area(Parent Process)
					close(fdM3M4[1]);
					close(fdM4SA[0]);

					while(counter > 0){
						for(i = 0; i < PIECE100; i++)
							read(fdM3M4[0], &tempPiece100[i], sizeof(piece));

						printf("Machine 4 packs 100 pieces\n");
						counter -= PIECE100;

						for(i = 0; i < PIECE100; i++)
							write(fdM4SA[1], &tempPiece100[i], sizeof(piece));
						}

					close(fdM3M4[0]);
					close(fdM4SA[1]);
					printf("Machine 4 shutting down...\n");

					return 0;

				default:
					return 0;
				}

			}else if(pid == -1){
				perror("Fork Failed");
				return -1;
			}

	}

	//Prints every pack of 100 pieces and waits for all of them to be produced
	piece tempPiece[SIZE];
	close(fdM4SA[1]);

	for(i = 0; i < SIZE; i++){
		read(fdM4SA[0], &tempPiece[i], sizeof(piece));
		printf("Pice nº %d\n", tempPiece[i].pieceID);
	}

	close(fdM4SA[0]);

	printf("The produced parts have been added to the inventory...");

	return 0;
}
