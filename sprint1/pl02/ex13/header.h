/*
 * header.h
 *
 *  Created on: 22/03/2020
 *      Author: isep
 */

#ifndef header_h
#define header_h

typedef struct{
 int pieceID;
 int length;
 int width;
 int height;
} piece;

#endif
