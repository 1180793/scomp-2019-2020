#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <sys/wait.h>
#include <time.h>

#define ARRAY_SIZE 1000
#define NUMBER_RANGE 200
#define RANGE 200
#define CHILDS 5

static const int LEITURA = 0;
static const int ESCRITA = 1;

int main(void) {

	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time(NULL));

	int vec1[ARRAY_SIZE];
	int vec2[ARRAY_SIZE];

	int i, totalExp = 0;
	for (i = 0; i < ARRAY_SIZE; i++) {
		vec1[i] = rand() % NUMBER_RANGE + 1;
		totalExp = totalExp + vec1[i];
	}
	for (i = 0; i < ARRAY_SIZE; i++) {
		vec2[i] = rand() % NUMBER_RANGE + 1;
		totalExp = totalExp + vec2[i];
	}
	printf("\n\nTotal Esperado: %d\n", totalExp);

	pid_t pid[CHILDS]; // Child pid vec
	int fd[2];

	if (pipe(fd) == -1) {
		perror("Pipe Failed!\n");
		return EXIT_FAILURE;
	}

	for (i = 0; i < CHILDS; i++) {
		pid[i] = fork();
		if (pid[i] == -1) {
			perror("Fork Failed!\n");
			return EXIT_FAILURE;
		} else if (pid[i] == 0) { // Código apenas Executado pelos Child
			int k, totalChild = 0;
			for (k = i * RANGE; k < (i + 1) *  RANGE; k++) {
				totalChild = totalChild + vec1[k] + vec2[k];
			}

			close(fd[LEITURA]);
			write(fd[ESCRITA], &(totalChild), sizeof(int));	// Escreve no pipe o total somado
			close(fd[ESCRITA]);

			exit(1);
		}
	}

	int k, status[CHILDS];
	for (k = 0; k < CHILDS; k++) {
		if (pid[k] > 0) {
			waitpid(pid[k], &status[k], 0); // Escreve no pipe o total somado
		}
	}

	int aux = 0, total = 0;
	close(fd[ESCRITA]);
	for (k = 0; k < CHILDS; k++) {
		if (WIFEXITED(status[k])) {
			read(fd[LEITURA], &aux, sizeof(int));
			total = total + aux;
		} else {
			perror("Child Exit Error!\n");
			return EXIT_FAILURE;
		}
	}
	close(fd[LEITURA]);

	printf("\nSoma Total: %d\n", total);

	return EXIT_SUCCESS;
}
