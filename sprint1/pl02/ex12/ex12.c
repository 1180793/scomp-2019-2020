#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

typedef struct{
	float price;
	char name[50];
} product;

typedef struct{
    char name[50];
    int id;
} request;

int main(void)
{
    int i;
    int fd[5][2];
    int fdUnique[2];
    struct product buffer_prod;
    struct request buffer_req;
    const int READ = 0;
    const int WRITE = 1;
    const int NUMBER_OF_CHILDREN = 5;

    if(pipe(fdUnique) == -1){
			perror("pipe falhou");
			exit(EXIT_FAILURE);
	}

    struct request products_r[5];
    
    products_r[0].id = 0;
    strcpy(products_r[0].name,"cereais");
    
    products_r[1].id = 1;
    strcpy(products_r[1].name,"sumo de laranja");
    
    products_r[2].id = 2;
    strcpy(products_r[2].name,"farinha");
    
    products_r[3].id = 3;
    strcpy(products_r[3].name,"alface");
    
    products_r[4].id = 4;
    strcpy(products_r[4].name,"maçã");
    
    for(i=0;i<5;i++){ //Criacao de processos filho(leitores de codigo de barras) e respetivos pipes
		
		if(pipe(fd[i]) == -1){
			
			perror("Erro na criação do pipe!");
			
			exit(EXIT_FAILURE);
		}	
		
		pid_t pid = fork();
		
		if(pid == -1){
			
			perror("Erro no fork!");
			
			exit(EXIT_FAILURE);
		}
		
		if(pid == 0){
			
			close(fd[i][WRITE]);
			
            write(fdUnique[WRITE],&products_r[i],sizeof(struct request)); //efetuar pedido ao processo pai
            
			if(read(fd[i][READ],&buffer,sizeof(struct product)) > 0){ //caso tenha sido efetuado um pedido imprime-se o produto
				
				printf("Codigo de barras para produto %d : nome: %s preco: %.2f \n",i+1,buffer.name, buffer.price);
			}
			
			close(fd[i][READ]);
			
			exit(EXIT_SUCCESS);
		}
		
    }  
    
    close(fdUnique[WRITE]);
    
    struct product produtos[5];
    
    produtos[0].preco = 2.0;
    strcpy(produtos[0].name,"cereais");
    
    produtos[1].preco = 1.98;
    strcpy(produtos[1].name,"sumo de laranja");
    
    produtos[2].preco = 2.33;
    strcpy(produtos[2].name,"farinha");
    
    produtos[3].preco = 0.83;
    strcpy(produtos[3].name,"alface");
    
    produtos[4].preco = 0.34;
    strcpy(produtos[4].name,"maçã");
    
    for(i=0; i<NUMBER_OF_CHILDREN; i++){
    	
        close(fd[i][READ]);
        
        if(read(fdUnique[READ],&buffer1,sizeof(struct request)) > 0){
        	
             write(fd[buffer1.id][WRITE],&produtos[i],sizeof(struct product));
        }
        
        close(fd[i][WRITE]);
    }

    close(fdUnique[WRITE]);
    
    return 0;
}
