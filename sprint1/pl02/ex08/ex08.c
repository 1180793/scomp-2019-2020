#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

static const int READ = 0;
static const int WRITE = 1;
static const int N_CHILDREN = 10;

typedef struct {

int num;
char *vencedor;

} jogo;

int main (void){
	
	int fd[2];
	jogo j;
	j.vencedor = "Win!";
	
	pipe(fd);	//Criacao do pipe.
	
	if(pipe(fd) == -1){	//Erro na criacao do pipe.
		perror("Erro na criacao do pipe!");
		return EXIT_FAILURE;
	}
	
	pid_t pid[N_CHILDREN];
	
	int i = 0;
	
	for (i=0; i<N_CHILDREN;i++){	//Criacao dos processos filhos.
		pid[i] = fork();
		
		if(pid[i] == 0){	//Se for filho
			close(fd[WRITE]);
			
			read(fd[READ],&j,sizeof(j));
			
			printf("Filho %d %s\n", getpid(), j.vencedor);	//Print do filho vencedor da ronda.
			close(fd[READ]);
			
			exit(j.num);
		
		}
	}
	
	close(fd[READ]);
	
	for(i=1;i<N_CHILDREN+1;i++){

		j.num = i;		
		write(fd[WRITE], &j, sizeof(j));
		sleep(2);
				
	}
			
	int saida;
			
	for (i=0;i<N_CHILDREN;i++){
		waitpid(pid[i], &saida,0);
		printf("O filho %d ganhou na ronda %d\n", pid[i], WEXITSTATUS(saida));	//Print o filho que ganhou e a certa ronda
	}
	close(fd[WRITE]);	
		
	return 0;
}
