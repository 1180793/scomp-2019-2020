/*
 * ex07.c
 *
 *  Created on: 21/03/2020
 *      Author: isep
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <sys/wait.h>
#include <time.h>
#include "ex07.h"

#define ARRAY_SIZE 1000
#define NUMBER_RANGE 200
#define RANGE 200
#define CHILDS 5

static const int LEITURA = 0;
static const int ESCRITA = 1;


/*
 * Preenche "aleatoriamente" um vetor passado como parametro
 * @*vec: apontador para um vetor de inteiros
 */
void fillArray(int *vec){
	int i ;
	for(i=0;i<ARRAY_SIZE;i++){
		vec[i]= rand() % NUMBER_RANGE +1; //numeros entre 1 e 200
	}
}

/**
 * Funçao a ser chamada pelo processo pai
 */
	void pai(int fd[CHILDS][2],int* vec, int i, pid_t pid){
		close(fd[i][ESCRITA]); //fecha a escrita que nao vai ser necessario

		int status;
		int j=0;
		int aux=0;

		waitpid(pid,&status,0); //pai fica a espera do filho

		if(WIFEXITED(status)){
			for(j= RANGE * i; j<RANGE * (i+1);j++){//percorre 200 posiçoes do array

				read(fd[i][LEITURA],&aux,sizeof(int)); //le o valor enviado pelo filho

				vec[j]= aux; // guarda o valor enviado pelo filho na posicao j do vetor


			}
		}
		close(fd[i][LEITURA]); // fecha a leitura do pipe que ja nao vai ser necessaria


	}
	/**
	 * Funçao a ser chamada pelos processos filhos
	 */
	void filho(int fd[CHILDS][2],int i, int *vec1,int *vec2){
		close(fd[i][LEITURA]); // Fecha a leitura do pipe

		int j =0;
		int aux=0;

		for(j=RANGE *i;j < RANGE * (i+1);j++){
			aux= vec1[j] + vec2[j]; // soma os valores das mesmas posiçoes de ambos
									// os vetores e guarda o resultado em aux

			write(fd[i][ESCRITA],&(aux),sizeof(aux)); // escreve no pipe o valor da soma


		}
		close(fd[i][ESCRITA]); // fecha a escrita do pipe
		exit(1); //termina o processo filho

	}

	int main(void){
		time_t t; // necessario para a geraçao aleatoria

		srand((unsigned)time(&t));

		//criar 2 vetores
		int vec1[ARRAY_SIZE];
		int vec2[ARRAY_SIZE];

		//preencher vetores
		fillArray(vec1);
		fillArray(vec2);

		int i;
		pid_t pid[CHILDS]; // vetor dos pids

		int vecTotal[ARRAY_SIZE]; // vetor do resultado final

		int fd[CHILDS][2];

		//criar pipes
		for(i=0;i<CHILDS;i++){
			if(pipe(fd[i])==-1){
				perror("Criaçao do pipe falhou");
				exit(-1);
			}
		}

		for(i=0;i<CHILDS;i++){
			pid[i]= fork(); // retorna o valor do pid do processo filho

			if(pid[i]==-1){
				perror("Erro na criaçao do processo filho!");
				exit(1);
			}else if(pid[i]==0){ // se for o filho
				filho(fd,i,vec1,vec2);
			}else{
				pai(fd,vecTotal,i,pid[i]);
			}

		}
		for(i=0;i<ARRAY_SIZE;i++){
			printf("VecSoma[%d] = %d ---> vec1[%d] = %d (+) vec2[%d] = %d\n",i,vecTotal[i],i,vec1[i],i,vec2[i]);
		}
		return 0;

	}



