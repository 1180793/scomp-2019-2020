#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>

static const int LEITURA = 0;
static const int ESCRITA = 1;

int main(void) {

	int fd[2];
	pid_t pidPai = 0;

	if (pipe(fd) == -1) {
		printf("Pipe Failed!\n");
		return EXIT_FAILURE;
	}

	pid_t pid;
	pid = fork();

	if (pid == -1) {
		printf("Fork Failed!\n");
		return EXIT_FAILURE;
	} else if (pid > 0) {	// Processo Pai

		close(fd[LEITURA]);	// Fecha extremidade nao usada

		pidPai = getpid();

		write(fd[1], &pidPai, sizeof(pidPai));

		close(fd[ESCRITA]);	// Fecha extremidade de escrita

	} else if (pid == 0) {

		close(fd[ESCRITA]); // Fecha extremidade nao usada
		pid_t pidLeitura;

		int rValue = read(fd[0], &pidLeitura, sizeof(pidPai));

		if (rValue > 0) {
			printf("PID Processo Pai : %d\n", pidLeitura);
		} else {
			printf("Error!\n");
			return EXIT_FAILURE;
		}

		close(fd[LEITURA]);  // Fecha extremidade de leitura

	}

	return EXIT_SUCCESS;

}
