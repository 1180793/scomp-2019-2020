/*
 * ex09.c
 *
 *  Created on: 21/03/2020
 *      Author: isep
 */


#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include "header.h"

#define SIZE 50000
#define N_CHILDS 10

int main(int n){

	int fd[2];
	int i;
	int j;
	int counter = 0;
	int search = SIZE / N_CHILDS;
	pid_t pid;
	sale salesArray[SIZE];

	/* Fill the array with random numbers
	 * (2000 possible product codes / Max quantity 30)
	 */
	for( i = 0; i < SIZE; i++){
		sale s;
		s.customer_code = random();
		s.product_code = random() % 2000;
		s.quantity = random() % 30;

		salesArray[i] = s;
	}

	if( pipe(fd) == -1){
		perror("Pipe failed");
		return -1;
	}

	for( i = 0; i < N_CHILDS; i++){

		pid = fork();

		if(pid == 0){
			int startIndex = i * search;

			/*The child process will perform a search within their corresponding range,
			sending the product code from any product with a quantity above 20 to the parent process*/
			close(fd[0]);
			for( j = startIndex; j < (startIndex + search); j++){

				if(salesArray[j].quantity > 20){

					write(fd[1], &salesArray[j].product_code, sizeof(int));
					counter++;
				}
			}

			close(fd[1]);
			return counter;
		}else if(pid == -1){
			perror("Fork Failed");
			return -1;
		}

	}

	//Waits for all the child processes to end
	for(i = 0; i < N_CHILDS; i++){
		wait(NULL);
	}

	//Records the product codes in the array product
	i = 0;

	int* product;

	product = (int*) malloc(sizeof(int));

	close(fd[1]);
	while(read(fd[0], &product[i], sizeof(int)) > 0){

		i++;

		product = (int*) realloc(product, (i + 1) * sizeof(int));
	}
	close(fd[0]);

	//Prints those codes
	for(int j = 0; j < i; j++){

		printf("Product code: %d\n", product[j] );

	}

	free(product);
	return 0;
}
