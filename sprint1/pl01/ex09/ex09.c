#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>

int main(){

	int i = 0;
	
	for(int i = 0;i<10;i++){ 
	
		int pos = 0;
	
		pid_t p = fork();
		
		if(p==0){ //se for filho
		
			int max = i*100 + 101; 
			
			for(int j=i*100;j < max;j++){
			
				printf("%d",j);
			}	
			
			exit(1);
		
		} else {
		 
		wait(NULL); //pai espera que todos os processos filho terminem
		
		}
		
	}

}