#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(void) {
	int i;
	pid_t pid;
	int status;

	for (i = 0; i < 4; i++) {
		pid = fork();
		if (pid == 0) {
			sleep(1); /*sleep(): unistd.h*/
			exit(i + 1);
		} else {
			waitpid(pid, &status, 0);
			if (WIFEXITED(status)) {
				printf("PID: %d | Order: %d\n", pid, WEXITSTATUS(status));
			}
		}
	}

	printf("This is the end.\n");

	return 0;
}
