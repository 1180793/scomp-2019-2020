#include systypes.h
#include unistd.h
#include stdio.h
#include stdlib.h
#include syswait.h

void main() {
	int i;
	pid_t pid;
	int status;
	
	for (i = 0; i  4; i++) {
		pid = fork();
		if (pid == 0) {
			sleep(1);
			break;
		} else if (pid % 2 == 0) {
			waitpid(pid, &status, 0);
		}
	}

	printf(This is the end.n);
}
