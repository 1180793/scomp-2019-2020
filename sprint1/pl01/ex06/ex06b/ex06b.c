#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

void main() {
	int i;
	pid_t pid;
	int status;
	for (i = 0; i < 4; i++) {
		pid = fork();
		if (pid == 0) {
			sleep(1);
			break;
		}
	}
	printf("This is the end.\n");
}
