#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(void) {

	int i = 0;
	int num = 2;
	pid_t pid[num]; // vetor onde serao guardados o valor de pid do processo filho(em caso de sucesso)

	pid[i] = fork();

	if(pid[i] == 0){
		sleep(1); // espera 1 segundo
		exit(1); // termina o processo filho e retorna o valor 1
	}

	i++;
	pid[i] = fork();

	if(pid[i] == 0){
		sleep(2); // espera 2 segundos
		exit(2); // termina o processo filho e retorna o valor 2
	}

	int status[num];

	for(i = 0; i < num; i++){
		waitpid(pid[i], &status[i], 0); //coloca o processo pai a espera do processo filho
		printf("%dº filho (pid = %d) : %d\n", i + 1, pid[i], WEXITSTATUS(status[i]));
	}

	return 0;
}



