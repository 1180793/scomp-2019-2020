Pai:
a=5, b=3115, c=3114, d=2992

Filho: 
a=5, b=0, c=3115, d=1387

Os valores de a são iguais para ambos os processos.
O valor de b será 0 para o processo filho ou o pid do processo filho criado pelo fork do processo pai.
O valor de c é o pid do processo. Para o pai será um valor inferior a b e para o filho será igual ao valor de b do processo pai.
