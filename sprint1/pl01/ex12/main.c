/*
 * main.c
 *
 *  Created on: 28/02/2020
 *      Author: isep
 *      ex12: ex12.o main.o
 */

#include <stdio.h>
#include "header.h"

int main(void){

	int nChilds = 6;

	int i = spawn_childs(nChilds);

	if( i != 0)
		printf("Filho at index nº%d\n", i * 2);
	else
		printf("Pai at index nº%d\n", i);

	return 0;
}

