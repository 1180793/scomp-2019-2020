/*
 * spawnchilds.c
 *
 *  Created on: 28/02/2020
 *  Author: isep
 *
 */

#include <stdio.h>
#include "header.h"
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h> /*necessário para a utilização do fork*/
#include <stdlib.h> /*necessário para a utilização do exit*/
#include <sys/wait.h>

int spawn_childs(int n){

	int i;
	pid_t pid;

	 for (i = 0; i < n; i++) {

		 pid = fork();

		 if( pid == 0){
			 return i+1;
		 }else if(pid == -1){
			 exit(-1);
		}
		 wait(NULL);
	 }

	return 0;
}

