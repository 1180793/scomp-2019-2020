#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>

#define ARRAY_SIZE 1000
int main() {
	int numbers[ARRAY_SIZE];	/* array to lookup */
	int n;			/* the number to find */
	time_t t;			/* needed to initialize random number generator (RNG) */
	int i;

	/* initializes RNG (srand():stdlib.h; time(): time.h) */
	srand ((unsigned) time (&t));

	/* initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < ARRAY_SIZE; i++)
		numbers[i] = rand () % 10000;

	/* initialize n */
	n = rand () % 10000;

	int count = 0;
	pid_t id = fork();
	if (id == 0) {
		for (i = 0; i < ARRAY_SIZE/2; i++) {
			if (numbers[i] == n) {
				count++;
			}
		}
		exit(count);
	} else {
		for (i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++) {
			if (numbers[i] == n) {
				count++;
			}
		}
	}

	int status;
	waitpid(id, &status, 0);
	if (WIFEXITED(status)) {
		count += WEXITSTATUS(status);
	}

	printf("The value %d is repeated %d times.", n, count);
}
