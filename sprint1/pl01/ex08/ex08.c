/*
 * ex08.c
 *
 *  Created on: 18/03/2020
 *      Author: isep
 */

#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h> /*necessário para a utilização do fork*/
#include <stdlib.h> /*necessário para a utilização do exit*/
#include <sys/wait.h>

	int main()
	{
		pid_t p;

		if (fork() == 0) {
			printf("PID = %d\n", getpid()); exit(0);
		}

		if ((p=fork()) == 0) {
			printf("PID = %d\n", getpid()); exit(0);
		}

		printf("Parent PID = %d\n", getpid());

		printf("Waiting... (for PID=%d)\n",p);
		waitpid(p, NULL, 0);

		printf("Enter Loop...\n");
		while (1); /* Infinite loop */
	}
