/*
 * ex10.c
 *
 *  Created on: 06/03/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>


int main(int n){

	int i;
	int j;
	pid_t pid[10];
	int status[10];
	int res[10];
	int randArray[2000];
	n = 15;
	for( i = 0; i < 2000; i++){

		randArray[i] = random() % 20;
		printf("%d - index: %d\n",randArray[i], i);
	}

	for(i = 0; i < 10; i++){

		pid[i] = fork();

		if( pid[i] == 0){

			for(j = 200 * (i+1) - 200; j < 200 * (i+1); j++){
				if(n == randArray[j]){
					exit(j - (200 * i));
				}
			}
			return 255;
		}else if(pid[i] == -1){
			exit(-1);
		}


	}

	for(i = 0; i < 10; i++){
		waitpid(pid[i],&status[i], 0);
		if(WIFEXITED(status[i]))
			res[i] = WEXITSTATUS(status[i]);
		printf("Number found at %d\n", res[i] + (200 * i));
	}

	return 0;
}
