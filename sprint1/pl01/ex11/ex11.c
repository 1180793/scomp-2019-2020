/*
 * ex11.c
 *
 *  Created on: 22/03/2020
 *      Author: isep
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

#define SIZE 1000
#define MAX_VALUE 255
#define N_CHILDS 5

int main(int n){

	int i;
	int j;
	int max = -1;
	int range = SIZE / N_CHILDS;
	int numbers[N_CHILDS];
	int result[N_CHILDS];
	pid_t pid[N_CHILDS];
	int status[N_CHILDS];
	int randArray[SIZE];

	for( i = 0; i < SIZE; i++){

		randArray[i] = random() % MAX_VALUE + 1;
		printf("Value for index %d: %d\n",i, randArray[i]);
	}

	for(i = 0; i < N_CHILDS; i++){

		pid[i] = fork();

		if( pid[i] == 0){

			int startIndex = range * i;

			for(j = startIndex; j < startIndex + range; j++){
				if(randArray[j] > max){
					max = randArray[j];
				}
			}
			return max;
		}else if(pid[i] == -1){
			perror("Fork Failed");
			return -1;
		}
	}

	for(i = 0; i < N_CHILDS; i++){
		waitpid(pid[i],&status[i], 0);
		if(WIFEXITED(status[i]))
			numbers[i] = WEXITSTATUS(status[i]);
	}

	for( i = 0; i < N_CHILDS; i++)
			printf("Max number for group %d: %d\n",i+1, numbers[i]);

	pid_t pid1;

	int startIndex1 = 0;
	int startIndex2 = N_CHILDS / 2;

	pid1 = fork();

	if( pid1 > 0){

		for( i = startIndex2; i < N_CHILDS; i++){
			result[i] = ((int) numbers[i]/MAX_VALUE) * 100;
		}
		wait(NULL);
		for( i = startIndex2; i < N_CHILDS; i++){
			printf("Result for group %d: %d\n", i+1, result[i]);
		}

	}else if( pid1 == 0){

		for( i = startIndex1; i < startIndex2; i++){
				result[i] = ((int) numbers[i]/MAX_VALUE) * 100;
				printf("Result for group %d: %d\n", i+1, result[i]);
			}
			return 0;
	}else{
		perror("Fork Failed");
		return -1;
	}

	return 0;
}
